
local composer = require( "composer" )

local scene = composer.newScene()

audio.reserveChannels(5)
local home



local function cambiaScena7 ()
composer.gotoScene( "menu" ) --si possono aggiungere effetti
end

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local cloud4
--local cloud=display.newImageRect("cloud.png",165,105)
--cloud.x=display.contentCenterX+20
--cloud.y=display.contentCenterY+20


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function fumetti(event)
if(event.phase=="began")
  then cloud.alpha=1
end
end

local function fumetti2(event)
if(event.phase=="began")
then cloud2.alpha=1
end
end

local function avviamusica()
audio.play(backsound,{channel=1,loops=-1})
end


local function fumetti3(event)
if(event.phase=="began")
then cloud3.alpha=1
end
end


local function fumetti4(event)
if(event.phase=="began")
then cloud4.alpha=1
end
end

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
  background = display.newImageRect("crediti2.png",770,430 ) --1024x600
  background.x = display.contentCenterX
  background.y = display.contentCenterY
  sceneGroup:insert(background)
  

cloud=display.newImageRect("nuvoletta.png",400,350)
cloud.x=display.contentCenterX-235
cloud.y=display.contentCenterY+20
cloud.alpha=0
cloud.isHitTestable=true
cloud:addEventListener("touch",fumetti)
sceneGroup:insert(cloud)


cloud2=display.newImageRect("nuvoletta2.png",400,350)
cloud2.x=display.contentCenterX-50
cloud2.y=display.contentCenterY+15
cloud2.alpha=0
cloud2.isHitTestable=true
cloud2:addEventListener("touch",fumetti2)
sceneGroup:insert(cloud2)


cloud3=display.newImageRect("nuvoletta3.png",400,350)
cloud3.x=display.contentCenterX+130
cloud3.y=display.contentCenterY+15
cloud3.alpha=0
cloud3.isHitTestable=true
cloud3:addEventListener("touch",fumetti3)
sceneGroup:insert(cloud3)

backsound=audio.loadStream("sini.wav")
cloud4=display.newImageRect("nuvoletta4.png",400,350)
cloud4.x=display.contentCenterX+312
cloud4.y=display.contentCenterY+15
cloud4.alpha=0
cloud4.isHitTestable=true
cloud4:addEventListener("touch",fumetti4)
sceneGroup:insert(cloud4)


-- playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
 -- playButton:setFillColor( 0, 0, 0 )
 -- sceneGroup:insert(playButton)

home=display.newImageRect("home3.png",80,80)
home.x=display.contentCenterX+344
home.y=display.contentCenterY-176
sceneGroup:insert(home)

home:addEventListener("tap",cambiaScena7)


 -- playButton:addEventListener( "tap", cambiaScena )

local hit=audio.loadSound('sini.wav')
audio.play(hit)
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

local hit=audio.loadSound('sini.wav')
audio.play(hit)
elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
local hit=audio.loadSound('sini.wav')
audio.play(hit)

end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
audio.stop()

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
audio.stop()
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
