
local composer = require( "composer" )
local scene = composer.newScene()
local physics = require( "physics" )
local perspective = require("perspective")
system.activate("multitouch")
local camera = perspective.createView(40)
physics.start()


local sheetOptions2 =
{
  frames = {

    {
      -- Slide (1)
      x=983,
      y=1,
      width=511,
      height=538,

      sourceX = 0,
      sourceY = 24,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (2)
      x=1492,
      y=545,
      width=507,
      height=540,

      sourceX = 0,
      sourceY = 22,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (3)
      x=1496,
      y=1,
      width=501,
      height=542,

      sourceX = 0,
      sourceY = 20,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (4)
      x=494,
      y=1083,
      width=497,
      height=544,

      sourceX = 0,
      sourceY = 18,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (5)
      x=1,
      y=551,
      width=491,
      height=546,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (6)
      x=1,
      y=1,
      width=487,
      height=548,

      sourceX = 0,
      sourceY = 14,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (7)
      x=490,
      y=1,
      width=491,
      height=546,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (8)
      x=993,
      y=1083,
      width=497,
      height=544,

      sourceX = 0,
      sourceY = 18,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (9)
      x=1492,
      y=1087,
      width=501,
      height=542,

      sourceX = 0,
      sourceY = 20,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (10)
      x=983,
      y=541,
      width=507,
      height=540,

      sourceX = 0,
      sourceY = 22,
      sourceWidth = 519,
      sourceHeight = 562
    },
  },

  sheetContentWidth = 2000,
  sheetContentHeight = 1630
}

local sheetOptions =
{
  frames = {

    {
      -- dead
      x=1215,
      y=1165,
      width=516,
      height=384,

    },
    {
      -- idle-1
      x=769,
      y=586,
      width=372,
      height=581,

    },
    {
      -- idle-2
      x=1143,
      y=584,
      width=372,
      height=579,

      sourceX = 0,
      sourceY = 2,
      sourceWidth = 372,
      sourceHeight = 581
    },
    {
      -- idle-3
      x=1505,
      y=1,
      width=372,
      height=577,

      sourceX = 0,
      sourceY = 4,
      sourceWidth = 372,
      sourceHeight = 581
    },
    {
      -- idle-4
      x=1131,
      y=1,
      width=372,
      height=581,

    },
    {
      -- jump
      x=1,
      y=1203,
      width=396,
      height=526,

    },
    {
      -- run-1
      x=393,
      y=592,
      width=374,
      height=587,

      sourceX = 24,
      sourceY = 14,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-2
      x=1,
      y=1,
      width=370,
      height=601,

      sourceX = 26,
      sourceY = 0,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-3
      x=399,
      y=1181,
      width=390,
      height=555,

      sourceX = 15,
      sourceY = 23,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-4
      x=1517,
      y=580,
      width=370,
      height=577,

      sourceX = 25,
      sourceY = 24,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-5
      x=373,
      y=1,
      width=368,
      height=589,

      sourceX = 26,
      sourceY = 12,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-6
      x=1,
      y=604,
      width=390,
      height=597,

      sourceX = 15,
      sourceY = 4,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-7
      x=791,
      y=1169,
      width=422,
      height=567,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-8
      x=743,
      y=1,
      width=386,
      height=583,

      sourceX = 18,
      sourceY = 18,
      sourceWidth = 422,
      sourceHeight = 601
    },
  },

  sheetContentWidth = 1888,
  sheetContentHeight = 1737
}

--spritesheet zombie male
local sheetOptions3 =
{
  frames = {

    {
      -- Idle (1)
      x=579,
      y=917,
      width=284,
      height=461,

      sourceX = 72,
      sourceY = 47,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (2)
      x=1,
      y=915,
      width=286,
      height=459,

      sourceX = 72,
      sourceY = 49,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (3)
      x=289,
      y=917,
      width=288,
      height=459,

      sourceX = 72,
      sourceY = 49,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (4)
      x=597,
      y=458,
      width=290,
      height=457,

      sourceX = 71,
      sourceY = 51,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (5)
      x=601,
      y=1,
      width=292,
      height=455,

      sourceX = 71,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (6)
      x=895,
      y=1,
      width=292,
      height=455,

      sourceX = 72,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (7)
      x=1189,
      y=1,
      width=294,
      height=455,

      sourceX = 72,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (8)
      x=1485,
      y=1,
      width=296,
      height=455,

      sourceX = 71,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (9)
      x=1,
      y=458,
      width=298,
      height=455,

      sourceX = 71,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (10)
      x=1,
      y=1,
      width=300,
      height=455,

      sourceX = 71,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (11)
      x=303,
      y=1,
      width=296,
      height=455,

      sourceX = 72,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (12)
      x=301,
      y=458,
      width=294,
      height=457,

      sourceX = 72,
      sourceY = 51,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (13)
      x=889,
      y=458,
      width=292,
      height=459,

      sourceX = 72,
      sourceY = 49,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (14)
      x=1183,
      y=458,
      width=290,
      height=459,

      sourceX = 71,
      sourceY = 49,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Idle (15)
      x=1475,
      y=458,
      width=288,
      height=459,

      sourceX = 71,
      sourceY = 49,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (1)
      x=865,
      y=919,
      width=284,
      height=461,

      sourceX = 72,
      sourceY = 47,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (2)
      x=663,
      y=1382,
      width=316,
      height=475,

      sourceX = 38,
      sourceY = 44,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (3)
      x=981,
      y=1382,
      width=336,
      height=475,

      sourceX = 16,
      sourceY = 41,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (4)
      x=325,
      y=1380,
      width=336,
      height=469,

      sourceX = 16,
      sourceY = 41,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (5)
      x=1,
      y=1378,
      width=322,
      height=467,

      sourceX = 32,
      sourceY = 44,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (6)
      x=1151,
      y=919,
      width=312,
      height=461,

      sourceX = 44,
      sourceY = 47,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (7)
      x=1765,
      y=458,
      width=274,
      height=477,

      sourceX = 78,
      sourceY = 42,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (8)
      x=1319,
      y=1390,
      width=280,
      height=479,

      sourceX = 68,
      sourceY = 39,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (9)
      x=1751,
      y=937,
      width=288,
      height=473,

      sourceX = 60,
      sourceY = 39,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Walk (10)
      x=1465,
      y=919,
      width=284,
      height=469,

      sourceX = 68,
      sourceY = 43,
      sourceWidth = 430,
      sourceHeight = 519
    },
  },

  sheetContentWidth = 2040,
  sheetContentHeight = 1870
}

local sheetOptions4 =
{
  frames = {

    {
      -- Attack (1)
      x=305,
      y=1084,
      width=270,
      height=457,

      sourceX = 72,
      sourceY = 51,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (2)
      x=1727,
      y=740,
      width=276,
      height=455,

      sourceX = 28,
      sourceY = 53,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (3)
      x=1455,
      y=740,
      width=270,
      height=451,

      sourceX = 58,
      sourceY = 57,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (4)
      x=386,
      y=651,
      width=348,
      height=431,

      sourceX = 71,
      sourceY = 78,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (5)
      x=1119,
      y=677,
      width=334,
      height=441,

      sourceX = 71,
      sourceY = 69,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (6)
      x=1712,
      y=289,
      width=318,
      height=449,

      sourceX = 72,
      sourceY = 61,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (7)
      x=1,
      y=1067,
      width=302,
      height=455,

      sourceX = 72,
      sourceY = 55,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Attack (8)
      x=988,
      y=1120,
      width=288,
      height=459,

      sourceX = 71,
      sourceY = 51,
      sourceWidth = 430,
      sourceHeight = 519
    },
    {
      -- Dead (1)
      x=1,
      y=641,
      width=383,
      height=424,

      sourceX = 0,
      sourceY = 42,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (2)
      x=577,
      y=1095,
      width=409,
      height=458,

      sourceX = 19,
      sourceY = 32,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (3)
      x=736,
      y=661,
      width=381,
      height=432,

      sourceX = 64,
      sourceY = 33,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (4)
      x=1363,
      y=289,
      width=347,
      height=386,

      sourceX = 62,
      sourceY = 71,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (5)
      x=1066,
      y=281,
      width=295,
      height=378,

      sourceX = 89,
      sourceY = 91,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (6)
      x=751,
      y=281,
      width=313,
      height=374,

      sourceX = 87,
      sourceY = 94,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (7)
      x=424,
      y=279,
      width=325,
      height=370,

      sourceX = 88,
      sourceY = 97,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (8)
      x=1,
      y=279,
      width=421,
      height=360,

      sourceX = 91,
      sourceY = 114,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (9)
      x=1498,
      y=1,
      width=493,
      height=286,

      sourceX = 94,
      sourceY = 186,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (10)
      x=999,
      y=1,
      width=497,
      height=278,

      sourceX = 93,
      sourceY = 192,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (11)
      x=1,
      y=1,
      width=497,
      height=276,

      sourceX = 94,
      sourceY = 197,
      sourceWidth = 629,
      sourceHeight = 526
    },
    {
      -- Dead (12)
      x=500,
      y=1,
      width=497,
      height=276,

      sourceX = 94,
      sourceY = 198,
      sourceWidth = 629,
      sourceHeight = 526
    },
  },

  sheetContentWidth = 2031,
  sheetContentHeight = 1580
}





-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local eroeSheet1 = graphics.newImageSheet( "spritesheet.png", sheetOptions )
local eroeSheet2 = graphics.newImageSheet( "spritesheet2.png", sheetOptions2 )
local zombieMaleSheet1 = graphics.newImageSheet( "spritesheet_z1.png", sheetOptions3 )
local zombieMaleSheet2 = graphics.newImageSheet( "spritesheet_z2.png", sheetOptions4 )
local centro_schermo = display.contentCenterX
local eroeJump
local terreno
local burrone
local spike
local percorso_zombieM
local percorso2_zombieM
local percorso3_zombieM
local percorso4_zombieM
local zombieM_direzione = 1
local zombieM2_direzione = 1
local piattaforma
local piattaforma2
local piattaforma3
local piattaforma4
local piattaforma5
local piattaforma6
local piattaforma7
local background1
local background2
local background3
local background4
local eroe
local zombieM
local zombieM2
local backGroup 
local mainGroup
local uiGroup
local controls_sx
local controls_dx
local jbutton
local sbutton
local life
local lifeCounter
local final_lifeCounter
local midground1
local midground2
local carica
local progresso = 2
local final_progresso
local tree
local tap = 0
local tapS = 0
local slideTimer
local sign_go
local sign_slide
local sign_jump
local sign_trick
local tomba_portale
local timerCambiaScena
local change = 0
local eroeIsMorto = 0
local crate
local crate2
local blank_margin_crate2
local crate3
local blank_margin_crate3
local blank_margin_piattaforma
local blank_margin_piattaforma2
local blank_margin_piattaforma3
local blank_margin_piattaforma4
local blank_margin_piattaforma5
local blank_margin_piattaforma6
local blank_margin_piattaforma7
local limiteLivello
local limiteLivello_precedente
local partitaVinta = 0
contaTouch = 0
collTerr = 0
verControl = 1
isSaltato = 0 -- serve per capire se ha saltato (per settare alcune proprieta)
inizioPartita = 0 -- serve per evitare accumuli di listener
isFermo = 1 -- serve a differenziare salto normale da salto con direzione
direzione = 0 -- serve per capire la direzione del salto (sx o dx)
premuto = 0
contaDelaySlide = 0
local currentObject
local checkpoint
local final_checkpoint
local blank_margin_tomba
local moved_attivo = 0


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------



local function getSequenceData()
  local sequenceData = {
    {
      name = "idle",
      sheet = eroeSheet1,
      frames = {2,3,4,5},
      time = 440,
      loopCount = 0,
    },

    {
      name = "run",
      sheet = eroeSheet1,
      frames = {7,8,9,10,11,12,13,14},
      time = 440,
      loopCount = 0,
    },

    {
      name = "jump",
      sheet = eroeSheet1,
      frames = {6},
      time = 440,
      loopCount = 0,
    },
    {
      name = "dead",
      sheet = eroeSheet1,
      frames = {1},
      time = 440,
      loopCount = 0,
    },
    {
      name = "slide",
      sheet = eroeSheet2,
      frames = {1,2,3,4,5,6,7,8,9,10},
      time = 1000,
      loopCount = 1,
    },
    --zombie male
    {
      name = "idle",
      sheet = zombieMaleSheet1,
      frames = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},
      time = 880,
      loopCount = 0,
    },

    {
      name = "walk",
      sheet = zombieMaleSheet1,
      frames = {16,17,18,19,20,21,22,23,24,25},
      time = 880,
      loopCount = 0,
    },
    {
      name = "attack",
      sheet = zombieMaleSheet2,
      frames = {1,2,3,4,5,6,7,8},
      time = 880,
      loopCount = 0,
    },

    {
      name = "dead_zm",
      sheet = zombieMaleSheet2,
      frames = {9,10,11,12,13,14,15,16,17,18,19,20},
      time = 880,
      loopCount = 1,
    },


  }
  return sequenceData
end

local function saveLife()
  composer.setVariable("final_lifeCounter", lifeCounter )
  composer.setVariable("final_progresso",progresso)
  composer.setVariable("final_checkpoint",checkpoint)
end

local function loadLife()
  lifeCounter = composer.getVariable("final_lifeCounter")
  progresso =  composer.getVariable("final_progresso")
  checkpoint =  composer.getVariable("final_checkpoint")
end

local function cambiaScena ()
  if change == 0 then
    
    if partitaVinta ~= 1 then
      lifeCounter = lifeCounter -1
    end
    saveLife()
    --se hai vinto
    if partitaVinta == 1 then
      progresso = progresso + 1--agg
      checkpoint = 0
      saveLife()--agg
      composer.gotoScene( "carica_livello" )
    
    --vedi altre situazioni...
  elseif lifeCounter == 0 then 
      lifeCounter = 3
      progresso = 1 --agg
      checkpoint = 0
      saveLife()
  
      if checkpoint == 0 then
        print("chiusura-->CHECKPOINT >>> 0")
      else
        print("chiusura-->CHECKPOINT >>> ESISTE!")
      end
      composer.gotoScene( "scena_sconfitta_definitiva" )
      
  else
      progresso = 2--agg
  
      saveLife()--agg
     
      composer.gotoScene( "scena_sconfitta" ) --si possono aggiungere effetti
   
  -- composer.gotoScene( "carica_livello" )
    end
    change = 1
  end
end


local function setProprietaEroe()
  eroe.speed = 300
end

local function setVelocitaEroe()
  eroe:setLinearVelocity(eroe.velocity, 0)

end


local stage = display.getCurrentStage()

-- funzione che determina il comportamento del pulsante del movimento e richiama 
-- anche la funzione del pulsante salto se premuto (multitouch)


local function vaiSx()
      if eroeIsMorto == 0 then --se l'eroe NON è morto
          if isSaltato == 0 then
              eroe:play()
              eroe.velocity = -eroe.speed  
              eroe.xScale = -0.3
              direzione = -1
              setVelocitaEroe()
          end
      end
end

local function onButtonTouchLeft(event)

  local phase = event.phase
  local button = event.target

  if eroeIsMorto == 1 then
    print("sono morto")
    button.isButtonPressed = false
    stage:setFocus(nil)
    return true
  end
  
  
  if event.phase == "began" then
    
      currentObject = event.target
  
      isFermo = 0
      print("premuto sx")
      premuto = 1
      direzione = -1
      eroe:pause()
      eroe:setSequence("run")
      eroe:play()
     display.getCurrentStage():setFocus( event.target )
     event.target.isFocus = true
    Runtime:addEventListener( "enterFrame", vaiSx )
   

 --   elseif event.phase == "moved" and event.target.isFocus then
      elseif event.phase == "moved" then 
          print("premuto moved sx: "..premuto)
          isFermo = 0
          tapS = 0 --se stai correndo abilita slide
          contaDelaySlide = 0
          direzione = -1
          
    if eroe.sequence == "idle" then
         eroe:pause()
         eroe:setSequence("run")
         eroe:play()
    end
      
    if moved_attivo == 0 then
      print(">>>ATTIVO RUNTIME SX<<<")
        Runtime:removeEventListener( "enterFrame", vaiSx )
        Runtime:addEventListener( "enterFrame", vaiSx )
        moved_attivo = 1
    end
          
        
    display.getCurrentStage():setFocus( nil )
    event.target.isFocus = false
    
    
    elseif event.phase == "ended" or event.phase == "cancelled" then
      print("rilasciato movimento")
      isFermo = 1
      direzione = 0
      premuto = 0
      attivo = 0
      contaDelaySlide = 0--agg
      if isFermo == 0 then
        eroe:setLinearVelocity( 0,0 ) 
      end
      moved_attivo = 0
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
      Runtime:removeEventListener( "enterFrame", vaiSx )
      display.getCurrentStage():setFocus( nil )
      event.target.isFocus = false

    
  end

  return true
end

local function vaiDx()
     
      if eroeIsMorto == 0 then --se l'eroe NON è morto
           if isSaltato == 0 then
              eroe:play()
              eroe.velocity = eroe.speed  
              eroe.xScale = 0.3
              direzione = 1
              setVelocitaEroe()
          end
      end
end

local function onButtonTouchRight(event)

  local phase = event.phase
  local button = event.target

  if eroeIsMorto == 1 then
    print("sono morto")
    button.isButtonPressed = false
    stage:setFocus(nil)
    return true
  end
  
  --if not eroe.sequence == "jump" then

  if event.phase == "began" then
      currentObject = event.target
    
      print("premuto dx")
      premuto = 1
      direzione = 1
      eroe:pause()
      eroe:setSequence("run")
      eroe:play()
   
      display.getCurrentStage():setFocus( event.target )
      event.target.isFocus = true
      
      Runtime:addEventListener( "enterFrame", vaiDx )
      
      
 --   elseif phase == "moved" and event.target.isFocus then
   elseif phase == "moved" then
    
      print("premuto moved dx: "..premuto)
      tapS = 0 --se stai correndo abilita slide
      contaDelaySlide = 0
      direzione = 1
      isFermo = 0
      
    if eroe.sequence == "idle" then
         eroe:pause()
         eroe:setSequence("run")
         eroe:play()
    end
     if moved_attivo == 0 then
        print(">>>ATTIVO RUNTIME DX<<<")
        Runtime:removeEventListener( "enterFrame", vaiDx )
        Runtime:addEventListener( "enterFrame", vaiDx )
        moved_attivo = 1
     end
   
     display.getCurrentStage():setFocus( nil )
     event.target.isFocus = false
      
    
    elseif event.phase == "ended" or phase == "cancelled" then
      
      print("rilasciato movimento")
      isFermo = 1
      direzione = 0
      premuto = 0
      attivo = 0
      contaDelaySlide = 0 --agg
      moved_attivo = 0
      if isFermo == 0 then
        eroe:setLinearVelocity( 0,0 ) 
      end
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
      
      Runtime:removeEventListener( "enterFrame", vaiDx )
      display.getCurrentStage():setFocus( nil )
      event.target.isFocus = false
  end

  return true
end



local function onLocalCollisionPlatform( self, event )
  
if event.other == eroe and event.other.isVisible == true then

  if ( event.phase == "began" ) then

      print("collisione platform inizio")
       eroe:pause()
       eroe:setSequence("idle")
       eroe:play()
      
     
      tap = 0
      print("iniziopartita: "..inizioPartita)
   
      if isSaltato == 1 and isFermo == 0 then
        eroe:setLinearVelocity(0,0) --togli accelerazione causata da salto + slide
        isSaltato = 0
        isFermo = 1
        contaDelaySlide = 0 
        --direzione = 0
        premuto = 0
        moved_attivo = 0
        if direzione == 1 then
        print("CON CHI CREDI DI AVERE A CHE FARE? -->DX")
        isSaltato = 0
        controls_dx:dispatchEvent( { name = "touch", target = controls_dx, phase = "moved" } )
         
        else
        isSaltato = 0
        print("CON CHI CREDI DI AVERE A CHE FARE?-->SX")
        controls_sx:dispatchEvent( { name = "touch", target = controls_sx, phase = "moved" } )
          
        end
      

      elseif isSaltato == 1 and isFermo == 1 then
    
        isSaltato = 0
        contaDelaySlide = 0 
      end

    
  elseif ( event.phase == "ended"  ) then

    print("collisione platform fine") 
     isSaltato = 1
    
    
    if collTerr == 0 and isSaltato == 0 then
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
    end
    tapS = 0
    contaDelaySlide = 0

  end
end

end

local function onLocalCollisionZombie( self, event )

  if ( event.phase == "began" ) then

   
    if event.other == eroe and event.other.isVisible == true then
      print("collisione zombie inizio")
      if eroe.sequence == "slide" then
        if self == zombieM then
        zombieM.anchorY = 0.40
        zombieM:pause()
        zombieM:setSequence("dead_zm")
        zombieM:play()
        contaDelaySlide = 0
        
        
         elseif self == zombieM2 then
          zombieM2.anchorY = 0.40
          zombieM2:pause()
          zombieM2:setSequence("dead_zm")
          zombieM2:play()
          contaDelaySlide = 0
        end
        

      else
        event.contact.bounce = 500  --era 400
        eroe.anchorY = 0.25
        eroe:pause()
        eroe:setSequence("dead")
        eroe:play()
        eroeIsMorto = 1
        rimuoviJbutton()
        rimuoviSbutton()
        rimuoviCollisioneBurrone()
        rimuoviCollisionePlatform()

        eroe.isFixedRotation = false

      end

    end
  


  elseif ( event.phase == "ended"  ) then

    print("collisione zombie fine")


  end
  -- return true
end



--funzione del pulsante salto, stabilisce i diversi comportamenti di salto, la variabile tap limita il numero di salti
--consecutivi
function jumpButton( event )
  if ( event.phase == "began" ) then
    print("INZIO FASE SALTO")
   
    display.getCurrentStage():setFocus( event.target )
    event.target.isFocus = true
    eroe:pause()
    eroe:setSequence("jump")
    eroe:play() 
  
    Runtime:removeEventListener( "enterFrame", vaiSx )
    Runtime:removeEventListener( "enterFrame", vaiDx )

  isSaltato = 1
  inizioPartita = 1
  print("-->isFermo: "..isFermo)

  if tap == 0 then
    print("direzione: "..direzione)

    if isFermo == 0 then
      if direzione < 0 then
        eroe.xScale = -0.3
        eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y)
      else
        eroe.xScale = 0.3
        eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y)
      end
    end
    if isFermo == 1 then
      eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y) 
    end
    tap = 1
  end
     
    elseif ( event.phase == "moved" ) then
 
        elseif event.phase == "ended" then
            print("FINE FASE SALTO")
            
            Runtime:removeEventListener( "enterFrame", vaiSx )
            Runtime:removeEventListener( "enterFrame", vaiDx )
            
            display.getCurrentStage():setFocus( nil )
            event.target.isFocus = false
        end

 
    return true
end

--sprite listener per eroe
local function spriteListener(event)

  if eroe.sequence == "slide" and eroe.frame == 10 then
    
    eroe:setSequence("idle")
    eroe:play()

    if slideTimer then
      print("cancello timer")
      timer.cancel(slideTimer)
      slideTimer = nil
    end
    slideTimer = timer.performWithDelay(200, attivaSlide,  1)
  end

  if eroe.sequence == "dead" then
    physics.removeBody(eroe)
    local nw, nh = eroe.width*0.3*0.5, eroe.height*0.3*0.5 --per custom shape
    physics.addBody(eroe, "dynamic", {density = 1.5, friction = 0.3, bounce=0, shape={-nw,-nh,nw,-nh,nw,nh-70,-nw,nh-70}})
    timerCambiaScena = timer.performWithDelay(2000, cambiaScena,  1) --era 2000
  end
  
    --hack per risolvere bug controlli su android 
    if eroe.sequence == "idle" or eroe.sequence == "jump" or eroe.sequence == "slide" then
        Runtime:removeEventListener( "enterFrame", vaiSx )
        Runtime:removeEventListener( "enterFrame", vaiDx )
    end
    
    if eroe.sequence == "run" then
      contaDelaySlide = 0
    end
    
    if eroe.sequence == "idle" then
      moved_attivo = 0
    end

end

local function spriteListenerZombie(event)
  if zombieM.sequence == "dead_zm" and zombieM.frame == 12 then
    print("elimina ZOMBIE")
    --     zombieM.isVisible= false
    zombieM.y = -2000
    --     physics.removeBody(zombieM)
    display.remove( zombieM )
    zombieM = nil

end

if zombieM ~=  nil then
    if zombieM.sequence == "dead_zm"  then
        zombieM:removeEventListener("collision")
    end
end

end

local function spriteListenerZombie2(event)
  if zombieM2.sequence == "dead_zm" and zombieM2.frame == 12 then
    print("elimina ZOMBIE2")
    --     zombieM.isVisible= false
    zombieM2.y = -2000
    --     physics.removeBody(zombieM)
    display.remove( zombieM2 )
    zombieM2 = nil
  end
  
if zombieM2 ~=  nil then
  if zombieM2.sequence == "dead_zm" then
    zombieM2:removeEventListener("collision")
  end
end

end


function attivaSlide()
  contaDelaySlide = 0
  print("attiva slide")
end


function slideButton( event )
  print("-->isFermoS: "..isFermo)
  print("-->tapS: "..tapS)
  print("-->isSaltatoS: "..isSaltato)
  print("-->contaDelaySlideS: "..contaDelaySlide)
  
  if ( event.phase == "began" ) then
    if isFermo == 0  and tapS == 0 and isSaltato == 0 and contaDelaySlide == 0 then --se non è fermo e non è in salto allora
    print("INZIO FASE SLIDE")
    display.getCurrentStage():setFocus( event.target )
    event.target.isFocus = true
    eroe:pause()
    eroe:setSequence("slide")
    eroe:play()
 
   Runtime:removeEventListener( "enterFrame", vaiDx )
   Runtime:removeEventListener( "enterFrame", vaisx )
    
    verContol = 0
  
    if direzione < 0 then
      eroe.xScale = -0.3
      eroe:applyLinearImpulse(-200,0,eroe.x,eroe.y)
    else
      eroe.xScale = 0.3
      eroe:applyLinearImpulse(200,0,eroe.x,eroe.y)
    end

    tapS = 1
    contaDelaySlide = 1
  
  end
      elseif ( event.phase == "moved" ) then
  
         elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
            print("FINE FASE SLIDE")
            Runtime:removeEventListener( "enterFrame", vaiDx )
            Runtime:removeEventListener( "enterFrame", vaisx )
            display.getCurrentStage():setFocus( nil )
            event.target.isFocus = false
        end
  
    return true
end



function fineLivelloCollision(self, event)
  if ( event.phase == "began" ) then


    if event.other == eroe and eroe.sequence ~= "dead"  then
      print("VAI A LIVELLO 2")
      partitaVinta = 1
      rimuoviJbutton()
      rimuoviSbutton()
      rimuoviCollisioneBurrone()
      rimuoviCollisionePlatform()
      cambiaScena ()
  end
  
  end

end

local function onLocalCollisionBurrone( self, event )

  if ( event.phase == "began" ) then


    if event.other == eroe and event.other.isVisible == true then
      
      rimuoviJbutton()
      rimuoviSbutton()
      rimuoviCollisioneBurrone()
      rimuoviCollisionePlatform()
      cambiaScena ()
      
    end
    
    
  end
  
end

local function onLocalCollsionPercorso (self, event)
  
    if ( event.phase == "began" ) then

        if event.other == zombieM and event.other.isVisible == true then
            print("COLLISIONE ZOMBIE")
            zombieM_direzione = - zombieM_direzione
            if zombieM_direzione == 0 then
              print("MUOVITI A SX") --toccato muro sx
              zombieM:setLinearVelocity(-100,0)
              zombieM.xScale = -0.3
              zombieM_direzione = 1
            else
               print("MUOVITI A DX")--toccato muro dx
              zombieM:setLinearVelocity(100,0)
              zombieM.xScale = 0.3
              zombieM_direzione = 0
            
          end
          
            elseif event.other == zombieM2 and event.other.isVisible == true then
            print("COLLISIONE ZOMBIE")
            zombieM2_direzione = - zombieM2_direzione
            if zombieM2_direzione == 0 then
              print("MUOVITI A SX") --toccato muro sx
              zombieM2:setLinearVelocity(-100,0)
              zombieM2.xScale = -0.3
              zombieM2_direzione = 1
            else
               print("MUOVITI A DX")--toccato muro dx
              zombieM2:setLinearVelocity(100,0)
              zombieM2.xScale = 0.3
              zombieM2_direzione = 0
            end
          
        end
  
    end

  
  
end

local function onLocalCollisionTomba (self, event)
  
    if ( event.phase == "began" ) then
        if event.other == eroe and checkpoint == 0 then
            checkpoint = 1
            composer.setVariable("final_checkpoint",checkpoint)
            display.remove(tomba_portale)
            tomba_portale = nil
          
            print("SETTATO CHECKPOINT!")
            
        end
      
    end
    
end

--funzioni per rimuovere e aggiungere l'event listener del pulsante del movimento per evitare di ricevere event quando l'eroe si trova nella fase di salto

function rimuoviControls()
  Runtime:removeEventListener( "enterFrame", vaiSx )
  Runtime:removeEventListener( "enterFrame", vaiDx )
  controls_dx:removeEventListener("touch", onButtonTouchRight )
  controls_sx:removeEventListener("touch", onButtonTouchLeft )
end

function rimuoviJbutton()
  jbutton:removeEventListener("touch", jumpButton )
end

function rimuoviSbutton()
  sbutton:removeEventListener("touch", slideButton )
end

function rimuoviCollisioneTerreno()
  terreno:removeEventListener( "collision" )
end
function rimuoviCollisioneBurrone()
  burrone:removeEventListener( "collision" )
end
function rimuoviCollisionePlatform()
  blank_margin_piattaforma:removeEventListener( "collision" )
  blank_margin_piattaforma2:removeEventListener( "collision" )
  blank_margin_piattaforma3:removeEventListener( "collision" )
  blank_margin_piattaforma4:removeEventListener( "collision" )
  blank_margin_piattaforma5:removeEventListener( "collision" )
  blank_margin_piattaforma6:removeEventListener( "collision" )
  blank_margin_crate2:removeEventListener( "collision" )
  blank_margin_crate3:removeEventListener( "collision" )
  blank_margin_tomba:removeEventListener( "collision" )


end

function attivaControls()
  controls_dx:addEventListener("touch", onButtonTouchRight )
  controls_sx:addEventListener("touch", onButtonTouchLeft )
end



--verifico se e' la seconda partita
carica = composer.getVariable("final_lifeCounter")
if(carica ~= nil) then
  loadLife()

else
  lifeCounter = 3
  final_lifeCounter = 0
  checkpoint = 0
  saveLife() --final_lifeCounter diventa 3
end

-- create()
function scene:create( event )


  physics.pause()  -- Temporarily pause the physics engine

  --display.setDefault("background", 0,0,255) --per cio che viene oltre lo sfondo

  local sceneGroup = self.view
  -- Code here runs when the scene is first created but has not yet appeared on screen
  -- Set up display groups
 -- local backGroup = display.newGroup()  -- Display group per sfondo
 -- local mainGroup = display.newGroup()  -- Display group per personaggio spari nemici etc oggetti
 -- local uiGroup = display.newGroup()    -- Display group for UI objects like the score
 
  --sceneGroup:insert( backGroup )  -- Insert into the scene's view group
  --sceneGroup:insert( mainGroup )  -- Insert into the scene's view group
 -- sceneGroup:insert( uiGroup )    -- Insert into the scene's view group

  display.setStatusBar(display.HiddenStatusBar)
  
  background1 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) --prima 800x600
  background1.x = display.contentCenterX-15
  background1.y = display.contentCenterY-200


  background2 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) 
  background2.x = display.contentCenterX-15 + background1.width
  background2.y = display.contentCenterY-200
  
  background3 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) --prima 800x600
  background3.x = display.contentCenterX-15 + background1.width + background2.width
  background3.y = display.contentCenterY-200


  background4 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) 
  background4.x = display.contentCenterX-15 + background1.width + background2.width + background3.width
  background4.y = display.contentCenterY-200

  burrone = display.newImageRect( sceneGroup, "vuoto.png", 7000, 10 ) --5000
  burrone.x = display.contentCenterX
  burrone.y = display.contentHeight-100-- -100

  --[[ 
 -- creo l'erore (per ora un rect) -->INSERIRE PER ANIMAZIONE E CAMBIO ANIMAZIONE
 -- eroe = display.newRect(0,0,60,50)
  eroe =   display.newImageRect("personaggio2.png",79,120) --invece di questo devi creare un animazione (idle), e poi diverse che vai a cambiare come toccapallone vedi ver dekstop, nota: un animazione puo avere anche solo una png!
  eroe.x = display.contentCenterX
  eroe.y = display.contentHeight-470
  eroe.isVisible = true
  --eroe.xScale = -1
  eroe.id = "eroe"
  --]]

--animazione eroe

  local eroeSequenze = getSequenceData()
  eroe = display.newSprite(sceneGroup, eroeSheet1, eroeSequenze)
  --eroe.x = display.contentCenterX-250 
  --eroe.y = display.contentHeight-600
  local scaleX = 0.3
  local scaleY = 0.3
  eroe:scale(scaleX,scaleY)
  local nw, nh = eroe.width*scaleX*0.5, eroe.height*scaleY*0.5 --per custom shape


  eroe.id = "eroe"
  eroe:play()
--eroe:setSequence("dead")

  --proprieta eroe
  setProprietaEroe()

  --creo i controlli per spostamento

  controls_sx = display.newImageRect("freccia_sx.png",73,70) --erano 53,50
  controls_sx.x = display.contentCenterX-340
  controls_sx.y = display.contentHeight-335
  controls_sx.id = "sx"
  
  controls_dx = display.newImageRect("freccia_dx.png",73,70)
  controls_dx.x = display.contentCenterX-200
  controls_dx.y = display.contentHeight-335
  controls_dx.id = "dx"

  --creo pulsanti per salto e slide (non li inserisco in sceneGroup)

  jbutton = display.newImageRect("jbutton_img.png",130,80)
  jbutton.x = display.contentCenterX+345
  jbutton.y = display.contentHeight-338
  jbutton.id = "jump"

  sbutton = display.newImageRect("sbutton_img.png",130,80)
  sbutton.x = display.contentCenterX+235
  sbutton.y = display.contentHeight-338
  sbutton.id = "slide"

  --creo interfaccia (non la inserisco in sceneGroup)
  life = display.newImageRect("ui_life.png",60,61)
  life.x = display.contentCenterX+260
  life.y = display.contentHeight-695
  life.id = "life"

  lifeText = display.newEmbossedText("x "..lifeCounter, 720, 345, "plasdrip.ttf", 40)
  lifeText:setFillColor( 243/255, 182/255, 58/255 )

  -- aggiungo oggetti a sfondo
 

  --[[
  blank_margin_crate = display.newImageRect(sceneGroup, "vuoto.png", 109, 10)
  blank_margin_crate.x = display.contentCenterX+350
  blank_margin_crate.y = display.contentHeight-506
  crate = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate.x = display.contentCenterX+350
  crate.y = display.contentHeight-450
  --]]
  
  
  
  
  blank_margin_piattaforma = display.newImageRect(sceneGroup, "vuoto.png", 382, 10)
  blank_margin_piattaforma.x = display.contentCenterX-185
  blank_margin_piattaforma.y = display.contentHeight-528
  piattaforma = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma.x = display.contentCenterX-185
  piattaforma.y = display.contentHeight-480
  
  blank_margin_piattaforma2 = display.newImageRect(sceneGroup, "vuoto.png", 378, 10)
  blank_margin_piattaforma2.x = display.contentCenterX+380 --+480
  blank_margin_piattaforma2.y = display.contentHeight-528
  piattaforma2 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma2.x = display.contentCenterX+380--+480
  piattaforma2.y = display.contentHeight-480
  
  blank_margin_piattaforma3 = display.newImageRect(sceneGroup, "vuoto.png", 382, 10)
  blank_margin_piattaforma3.x = display.contentCenterX+950
  blank_margin_piattaforma3.y = display.contentHeight-528
  piattaforma3 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma3.x = display.contentCenterX+950
  piattaforma3.y = display.contentHeight-480
  
  blank_margin_piattaforma4 = display.newImageRect(sceneGroup, "vuoto.png", 378, 10)
  blank_margin_piattaforma4.x = display.contentCenterX+1500
  blank_margin_piattaforma4.y = display.contentHeight-428
  piattaforma4 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma4.x = display.contentCenterX+1500
  piattaforma4.y = display.contentHeight-380
  
  blank_margin_piattaforma5 = display.newImageRect(sceneGroup, "vuoto.png", 378, 10)
  blank_margin_piattaforma5.x = display.contentCenterX+2000
  blank_margin_piattaforma5.y = display.contentHeight-528
  piattaforma5 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma5.x = display.contentCenterX+2000
  piattaforma5.y = display.contentHeight-480
  
  blank_margin_piattaforma6 = display.newImageRect(sceneGroup, "vuoto.png", 378, 10)
  blank_margin_piattaforma6.x = display.contentCenterX+2500
  blank_margin_piattaforma6.y = display.contentHeight-328
  piattaforma6 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma6.x = display.contentCenterX+2500
  piattaforma6.y = display.contentHeight-280
  
  blank_margin_crate2 = display.newImageRect(sceneGroup, "vuoto.png", 103, 10)
  blank_margin_crate2.x = display.contentCenterX+3000
  blank_margin_crate2.y = display.contentHeight-436
  crate2 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate2.x = display.contentCenterX+3000
  crate2.y = display.contentHeight-380
  
  blank_margin_crate3 = display.newImageRect(sceneGroup, "vuoto.png", 103, 10)
  blank_margin_crate3.x = display.contentCenterX+3300
  blank_margin_crate3.y = display.contentHeight-436
  crate3 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate3.x = display.contentCenterX+3300
  crate3.y = display.contentHeight-380
  
  --[[
  blank_margin_piattaforma6 = display.newImageRect(sceneGroup, "vuoto.png", 378, 10)
  blank_margin_piattaforma6.x = display.contentCenterX+2000
  blank_margin_piattaforma6.y = display.contentHeight-528
  piattaforma6 = display.newImageRect(sceneGroup, "piattaforma.png", 384, 93) 
  piattaforma6.x = display.contentCenterX+2000
  piattaforma6.y = display.contentHeight-480
  --]]
  
  spike = display.newImageRect(sceneGroup, "DeadBush.png", 132, 74) 
  spike.x = display.contentCenterX+950
  spike.y = display.contentHeight-668
  
  crate = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate.x = display.contentCenterX+950
  crate.y = display.contentHeight-562
  
  tomba_portale = display.newImageRect(sceneGroup, "tomb_bonus.png", 53, 76) 
  tomba_portale.x = display.contentCenterX+1650
  tomba_portale.y = display.contentHeight-465
  
  blank_margin_tomba = display.newImageRect(sceneGroup, "vuoto.png", 53, 76) 
  blank_margin_tomba.x = display.contentCenterX+1650
  blank_margin_tomba.y = display.contentHeight-465
  
  sign_trick = display.newImageRect(sceneGroup, "trick_sign.png", 91, 93) 
  sign_trick.x = display.contentCenterX+1550
  sign_trick.y = display.contentHeight-470
  
  sign_go = display.newImageRect(sceneGroup, "sign_go.png", 91, 93) 
  sign_go.x = display.contentCenterX+3300
  sign_go.y = display.contentHeight-470
  
  -- aggiungo nemico
  
  local zombieMSequenze = getSequenceData()
  zombieM = display.newSprite(sceneGroup, zombieMaleSheet1, zombieMSequenze)
  zombieM.x = display.contentCenterX+500 --+600
  zombieM.y = display.contentHeight-600
  zombieM:scale(scaleX,scaleY)
  local nw_z, nh_z = zombieM.width*scaleX*0.5, zombieM.height*scaleY*0.5 --per custom shape
 -- zombieM:setSequence("idle")
  zombieM:setSequence("walk")
  zombieM.xScale = -0.3
  zombieM:play()
  
  --aggiungo zombieM2
  
  zombieM2 = display.newSprite(sceneGroup, zombieMaleSheet1, zombieMSequenze)
  zombieM2.x = display.contentCenterX+2000
  zombieM2.y = display.contentHeight-600
  zombieM2:scale(scaleX,scaleY)
 -- zombieM:setSequence("idle")
  zombieM2:setSequence("walk")
  zombieM2.xScale = -0.3
  zombieM2:play()
 
  --setto percorso zombieM
  percorso_zombieM = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso_zombieM.x = display.contentCenterX+190--+290
  percorso_zombieM.y = display.contentHeight-550
  percorso2_zombieM = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso2_zombieM.x = display.contentCenterX+575--+675
  percorso2_zombieM.y = display.contentHeight-550
  
  --setto percorso zombieM2
  percorso_zombieM2 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso_zombieM2.x = display.contentCenterX+1810
  percorso_zombieM2.y = display.contentHeight-550
  percorso2_zombieM2 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso2_zombieM2.x = display.contentCenterX+2190
  percorso2_zombieM2.y = display.contentHeight-550
  
  
  --aggiungo limite livello
  limiteLivello= display.newImageRect(sceneGroup, "vuoto.png", 10, 1000)
  limiteLivello.x = display.contentCenterX+3400
  limiteLivello.y = display.contentHeight-450
  
  limiteLivello_precedente= display.newImageRect(sceneGroup, "vuoto.png", 10, 1000)
  limiteLivello_precedente.x = display.contentCenterX-375
  limiteLivello_precedente.y = display.contentHeight-450
  
  
  --aggiungo gli oggetti alla sceneGroup nel giusto ordine, back to front

  sceneGroup:insert( background1 )
	sceneGroup:insert( background2 )
  sceneGroup:insert( burrone )
  sceneGroup:insert( spike )
	sceneGroup:insert( blank_margin_piattaforma )
  sceneGroup:insert( piattaforma )
  sceneGroup:insert( blank_margin_piattaforma2 )
  sceneGroup:insert( piattaforma2 )
  sceneGroup:insert( blank_margin_piattaforma3 )
  sceneGroup:insert( piattaforma3 )
  sceneGroup:insert( blank_margin_piattaforma4 )
  sceneGroup:insert( piattaforma4 )
  sceneGroup:insert( blank_margin_piattaforma5 )
  sceneGroup:insert( piattaforma5 )
  sceneGroup:insert( blank_margin_piattaforma6 )
  sceneGroup:insert( piattaforma6 )
  sceneGroup:insert( blank_margin_crate2 )
  sceneGroup:insert( crate2 )
  sceneGroup:insert( blank_margin_crate3 )
  sceneGroup:insert( crate3 )
  sceneGroup:insert( crate )
  sceneGroup:insert( tomba_portale )
  sceneGroup:insert( blank_margin_tomba )
  sceneGroup:insert( sign_trick )
  sceneGroup:insert( sign_go )
  sceneGroup:insert( limiteLivello )
  sceneGroup:insert( limiteLivello_precedente )
  sceneGroup:insert( percorso_zombieM )
  sceneGroup:insert( percorso2_zombieM )
  sceneGroup:insert( percorso_zombieM2 )
  sceneGroup:insert( percorso2_zombieM2 )
  sceneGroup:insert( zombieM )
  sceneGroup:insert( zombieM2 )
	sceneGroup:insert( eroe )

 -- sceneGroup:insert( life )
--  sceneGroup:insert( lifeText )
--  sceneGroup:insert( controls)
--	sceneGroup:insert( jbutton )
--  sceneGroup:insert( sbutton )
  -- non li inserisco, quindi li rimuovo manulamente in scene:hide

  --aggiungo camera, più piccolo il layer più è vicino
  -- se aggiungi ricorda di cambiare il limite da 12 layer
    camera:add(eroe,1,true) -- layer 1 in focus
    camera:add(burrone,2,false) -- layer 2 non in focus
    camera:add(percorso_zombieM,3,false)
    camera:add(percorso2_zombieM,4,false)
    camera:add(percorso_zombieM2,5,false)
    camera:add(percorso2_zombieM2,6,false)
    camera:add(limiteLivello_precedente,7,false)
    camera:add(piattaforma2,8,false)
    camera:add(blank_margin_piattaforma2,9,false)
    camera:add(piattaforma,10,false)
    camera:add(zombieM,11,false)
    camera:add(zombieM2,12,false)
    camera:add(blank_margin_piattaforma,13,false)
    camera:add(blank_margin_piattaforma3,14,false)
    camera:add(piattaforma3,15,false)
    camera:add(blank_margin_piattaforma4,16,false)
    camera:add(piattaforma4,17,false)
    camera:add(blank_margin_piattaforma5,18,false)
    camera:add(piattaforma5,19,false)
    camera:add(blank_margin_piattaforma6,20,false)
    camera:add(piattaforma6,21,false)
    camera:add(blank_margin_crate2,22,false)
    camera:add(crate2,23,false)
    camera:add(blank_margin_crate3,24,false)
    camera:add(crate3,25,false)
    camera:add(spike,26,false)
    camera:add(crate,27,false)
    camera:add(tomba_portale,28,false)
    camera:add(blank_margin_tomba,29,false)
    camera:add(sign_trick,30,false)
    camera:add(sign_go,31,false)
    camera:add(limiteLivello,32,false)
    camera:add(background1,33,false) -- layer 3 non in focus
    camera:add(background2,34,false) -- layer 3 non in focus
    camera:add(background3,35,false) -- layer 3 non in focus
    camera:add(background4,36,false) -- layer 3 non in focus
    
    
 -- camera:layer(12).parallaxRatio = 0.5--terreno si muove piu veloce dello sfondo effetto 3d
  local levelWidth = camera:layer(2).width -- il layer 2 è quello che determina lunghezza lvl, ovvero il terreno
  camera:setBounds(display.contentWidth/2,3350, 0 , display.contentHeight-500)

  -- aggiungo fisica
  physics.addBody(burrone, "static", { bounce=0,friction = 1})
  burrone.isSensor = true
  physics.addBody(eroe, "dynamic", {density = 1.5, friction = 0.7, bounce=0, shape={-nw+50,-nh+20,nw-10,-nh+20,nw-30,nh,-nw+60,nh}})
 
  eroe.isFixedRotation = true
  physics.addBody(zombieM, "dynamic", {density = 10, friction = 0, bounce=0, shape={-nw+15,-nh+20,nw-20,-nh+20,nw-20,nh-22,-nw+15,nh-22}})
  zombieM.isFixedRotation = true
  zombieM.anchorY = 0.55
  --zombieM2
  
   physics.addBody(zombieM2, "dynamic", {density = 10, friction = 0, bounce=0, shape={-nw+15,-nh+20,nw-20,-nh+20,nw-20,nh-22,-nw+15,nh-22}})
  zombieM2.isFixedRotation = true
  zombieM2.anchorY = 0.55
  
  
  -- se muore zombieM.anchorY = 0.45
  physics.addBody(spike,"static", { shape={60,60,-60,0,0,-30,60,0,-60,60} } )
  physics.addBody(blank_margin_piattaforma,"static")
  physics.addBody(piattaforma,"static", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_piattaforma2,"static")
  physics.addBody(percorso_zombieM,"kinematic")
  physics.addBody(percorso2_zombieM,"kinematic")
  physics.addBody(percorso_zombieM2,"kinematic")
  physics.addBody(percorso2_zombieM2,"kinematic")
  physics.addBody(piattaforma2,"static", { bounce=0,friction = 1 } )
  physics.addBody(piattaforma3,"static", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_piattaforma3,"static")
  physics.addBody(piattaforma4,"static", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_piattaforma4,"static", { bounce=0,friction = 1 })
  physics.addBody(piattaforma5,"static", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_piattaforma5,"static", { bounce=0,friction = 1 })
  physics.addBody(piattaforma6,"static", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_piattaforma6,"static", { bounce=0,friction = 1 })
  physics.addBody(crate2,"static", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate2,"static", { bounce=0,friction = 3 })
  physics.addBody(crate3,"static", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate3,"static", { bounce=0,friction = 3 })
  physics.addBody(blank_margin_tomba,"static", { bounce=0,friction = 0 })
  blank_margin_tomba.isSensor = true
  blank_margin_piattaforma.isSensor = true
  blank_margin_piattaforma2.isSensor = true
  blank_margin_piattaforma3.isSensor = true
  blank_margin_piattaforma4.isSensor = true  
  blank_margin_piattaforma5.isSensor = true 
  blank_margin_piattaforma6.isSensor = true 
  blank_margin_crate2.isSensor = true 
  blank_margin_crate3.isSensor = true 
  percorso_zombieM.isSensor = true
  percorso2_zombieM.isSensor = true
  percorso_zombieM2.isSensor = true
  percorso2_zombieM2.isSensor = true
  physics.addBody(crate,"static", { bounce=0,friction = 1,  shape={-55,-60,-55,55,55,55,55,-60,} } ) --quadrato, centro  v = (0,0)
  physics.addBody(limiteLivello,"static")
  limiteLivello.isSensor = true
  physics.addBody(limiteLivello_precedente,"static")

  eroe.gravityScale = 8
  crate.anchorY = 0.68
  
  --settiamo velocita zombie
  zombieM:setLinearVelocity(-100,0)
  zombieM2:setLinearVelocity(-100,0)
 

 --PER DEBUG MEMORY LEAK
 -- timerCambiaScena = timer.performWithDelay(2000, cambiaScena,  1)
  

end


-- show()
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase


  if ( phase == "will" ) then
    -- Code here runs when the scene is still off screen (but is about to come on screen)
     
      if checkpoint == 0 then
        print("CHECKPOINT >>> 0")
      else
        print("CHECKPOINT >>> ESISTE!")
      end

      if checkpoint == 0 then
          eroe.x = display.contentCenterX-250 -- -250
          eroe.y = display.contentHeight-600
      else
          eroe.x = display.contentCenterX+1600 -- -250
          eroe.y = display.contentHeight-465
          display.remove(tomba_portale)
          tomba_portale = nil
      end
        
  
elseif ( phase == "did" ) then
    print( collectgarbage( "count" ) / 1024 ) -- default is to show you Kilobytes of memory, this converts it to Megabytes of memory
    print( system.getInfo( "textureMemoryUsed" ) / (1024 * 1024) ) -- defaults to bytes, this converts it to Megabytes.
    print("**********BENVENUTO A LEVEL2**********")
    physics.start()
    camera:track()
    --physics.setDrawMode( "hybrid" )
    controls_sx:addEventListener("touch", onButtonTouchLeft )
    controls_dx:addEventListener("touch", onButtonTouchRight )
    jbutton:addEventListener("touch", jumpButton )
    sbutton:addEventListener("touch", slideButton )
    eroe:addEventListener("sprite",spriteListener)
    
    burrone.collision = onLocalCollisionBurrone
    burrone:addEventListener( "collision" )
   
    blank_margin_tomba.collision = onLocalCollisionTomba
    blank_margin_tomba:addEventListener( "collision" )
    blank_margin_piattaforma.collision = onLocalCollisionPlatform
    blank_margin_piattaforma:addEventListener( "collision" )
    blank_margin_piattaforma2.collision = onLocalCollisionPlatform
    blank_margin_piattaforma2:addEventListener( "collision" )
    blank_margin_piattaforma3.collision = onLocalCollisionPlatform
    blank_margin_piattaforma3:addEventListener( "collision" )
    blank_margin_piattaforma4.collision = onLocalCollisionPlatform
    blank_margin_piattaforma4:addEventListener( "collision" )
    blank_margin_piattaforma5.collision = onLocalCollisionPlatform
    blank_margin_piattaforma5:addEventListener( "collision" )
    blank_margin_piattaforma6.collision = onLocalCollisionPlatform
    blank_margin_piattaforma6:addEventListener( "collision" )
    blank_margin_crate2.collision = onLocalCollisionPlatform
    blank_margin_crate2:addEventListener( "collision" )
    blank_margin_crate3.collision = onLocalCollisionPlatform
    blank_margin_crate3:addEventListener( "collision" )
    percorso_zombieM.collision = onLocalCollsionPercorso
    percorso_zombieM:addEventListener( "collision" )
    percorso2_zombieM.collision = onLocalCollsionPercorso
    percorso2_zombieM:addEventListener( "collision" )
    percorso_zombieM2.collision = onLocalCollsionPercorso
    percorso_zombieM2:addEventListener( "collision" )
    percorso2_zombieM2.collision = onLocalCollsionPercorso
    percorso2_zombieM2:addEventListener( "collision" )
    zombieM.collision = onLocalCollisionZombie
    zombieM:addEventListener("collision")
    zombieM:addEventListener("sprite",spriteListenerZombie)
    zombieM2.collision = onLocalCollisionZombie
    zombieM2:addEventListener("collision")
    zombieM2:addEventListener("sprite",spriteListenerZombie2)
    limiteLivello.collision = fineLivelloCollision
    limiteLivello:addEventListener("collision")
    
    
    Runtime:addEventListener( "touch", function(e)
      if currentObject ~= nil then
        if currentObject.contentBounds ~= nil then
          
             if e.x < currentObject.contentBounds.xMin or
                e.x > currentObject.contentBounds.xMax or
                e.y < currentObject.contentBounds.yMin or
                e.y > currentObject.contentBounds.yMax then
                print("Its out")--]]
                Runtime:removeEventListener( "enterFrame", vaiDx )
                Runtime:removeEventListener( "enterFrame", vaiSx )
      
           end
        end
      end
end)
    

    -- Code here runs when the scene is entirely on screen

  end
end


-- hide()
function scene:hide( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
    -- Code here runs when the scene is on screen (but is about to go off screen)

    if timerCambiaScena ~=  nil then
      timer.cancel(timerCambiaScena)
      timerCambiaScena = nil
    end
    if timerSlide ~= nil then
      timer.cancel(timerSlide)
      timerSlide = nil
    end
  

  elseif ( phase == "did" ) then
    -- Code here runs immediately after the scene goes entirely off screen
    -- physics.pause()
    physics.stop()
    rimuoviControls()
    eroe:removeEventListener("sprite",spriteListener)
    blank_margin_piattaforma:removeEventListener( "collision" )
    percorso_zombieM:removeEventListener( "collision" )
    percorso2_zombieM:removeEventListener( "collision" )
    percorso_zombieM2:removeEventListener( "collision" )
    percorso2_zombieM2:removeEventListener( "collision" )
    if zombieM ~=  nil then
      zombieM:removeEventListener("collision")
      zombieM:removeEventListener("sprite",spriteListenerZombie)
    end
    if zombieM2 ~=  nil then
      zombieM2:removeEventListener("collision")
      zombieM2:removeEventListener("sprite",spriteListenerZombie)
    end
    limiteLivello:removeEventListener("collision")
    Runtime:removeEventListener( "touch", function(e)end)
    
    --rimuovo manualmente GUI
    display.remove(life)
    life = nil
    display.remove(lifeText)
    lifeText = nil
    display.remove(controls_sx)
    controls_sx = nil
    display.remove(controls_dx)
    controls_dx = nil
    display.remove(jbutton)
    jbutton = nil
    display.remove(sbutton)
    sbutton = nil
    --rimuovo camera
    camera:destroy()
    camera=nil
    --rimuovo scena
    composer.removeScene( "level2" )
    

  end
end


-- destroy()
function scene:destroy( event )

  local sceneGroup = self.view
  -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
