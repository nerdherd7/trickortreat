
local composer = require( "composer" )

local scene = composer.newScene()

local perspective=require("perspective")
local camera=perspective.createView()


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background1
local background2
local midground1
local midground2
local backGroup
local mainGroup
local uigroup
local terreno
local physics=require("physics")
local personaggio
local zucca
local pad
local buttondx
local score=0
local fanta
physics.start()
local myText = display.newText( score,display.contentCenterX+350,display.contentCenterY-170, native.systemFont, 76 )
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni
local function movimentoDX(event)

personaggio:setLinearVelocity( 100, 0 )

end

local function punteggio(self,event)

if(event.phase =="began") then

if(event.other.myName == "zucca") then
score=score+1
myText.text=score

display.remove(zucca)
elseif(event.other.myName=="ghost") then
display.remove(fanta)
score=score-1
myText.text=score
end
end
end
-- create()
function scene:create( event )

  physics.pause()
  display.setDefault("background",0,0,255)

local backgroup=display.newGroup()
local mainGroup=display.newGroup()
local uiGroup=display.newGroup()
local sceneGroup = self.view

backGroup=display.newGroup()
sceneGroup:insert(backGroup)
 mainGroup=display.newGroup()
sceneGroup:insert(mainGroup)
uiGroup=display.newGroup()
sceneGroup:insert(uiGroup)
	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
  background1 = display.newImageRect(backGroup,"chapter1.png",800,600) --1024x600
  background1.x = display.contentCenterX-15
  background1.y = display.contentCenterY-75

background2 = display.newImageRect(backGroup,"chapter1.png",800,600) --1024x600
background2.x = display.contentCenterX-15+background1.width
background2.y = display.contentCenterY-75

midground1 = display.newImageRect(backGroup,"chapter1.png",800,600) --1024x600
midground1.x = display.contentCenterX-15
midground1.y = display.contentCenterY-75

midground2 = display.newImageRect(backGroup,"chapter1.png",800,600) --1024x600
midground2.x = display.contentCenterX-15+midground1.width
midground2.y = display.contentCenterY-75
 -- playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
 -- playButton:setFillColor( 0, 0, 0 )
 -- sceneGroup:insert(playButton)
  


 -- playButton:addEventListener( "tap", cambiaScena )

muro = display.newImageRect( backGroup, "terreno.png", display.contentWidth*3, 700 )
muro.x = display.contentCenterX
muro.y = display.contentHeight-25



   personaggio=display.newImageRect("personaggio.png", 100,110 )
   personaggio.x=display.contentCenterX-300
   personaggio.y=display.contentCenter
   personaggio.myName = "personaggio"
mainGroup:insert(personaggio)
   
   zucca=display.newImageRect("zucca.png", 50,60)
   zucca.x=display.contentCenterX
   zucca.y=display.contentCenterY
   zucca.myName="zucca"
   
   physics.start()
   physics.addBody(zucca, "dynamic")
   
   pad= display.newImageRect("pdafinale.png",200,240)
      pad.x=display.contentCenterX+325
   pad.y=display.contentCenterY+160
buttondx= display.newRect(display.contentCenterX+366,display.contentCenterY+160,30,30)
 physics.addBody(personaggio,"dynamic")
fanta= display.newImageRect("stopmusic.png",150,150)
fanta.x=display.contentCenterX+325
   fanta.y=display.contentCenterY+45
   physics.addBody(fanta,"static")
   fanta.myName="ghost"
   --myText:setReferencePoint(display.topLeftReferencePoint)

camera:add(personaggio,1,true)
camera:add(muro,2,false)
camera:add(midground1,3,false)
camera:add(midground2,3,false)
camera:layer(3).parallelaxRatio=0.8
camera:add(background1,3,false)
camera:add(background2,4,false)
camera:layer(4).parallelaxRatio=0.5
local levelWidth = camera:layer(2).width -- il layer 2 è quello che determina lunghezza lvl

camera:setBounds(display.contentWidth/2,levelWidth - 1155, 0 , display.contentHeight-500)
physics.addBody(muro,"static")
physics.addBody(personaggio,"dynamic")end
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
--	physics.start()

		-- Code here runs when the scene is still off screen (but is about to come on screen)
		    personaggio.collision=punteggio
personaggio:addEventListener("collision")
buttondx:addEventListener("touch",movimentoDX)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
		physics.start()

camera:track()

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
