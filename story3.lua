local composer = require( "composer" )
local physics = require( "physics" )
local scene = composer.newScene()
physics.start()
local player
local cloud1
local timerSource

display.setStatusBar(display.HiddenStatusBar)


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local playbutton
local playcredit
local newGameButton
local newGameButton2
local newGameButton3
local appmusic=0
local chapter



local sheetOptions =
{
width = 128,
height = 128,
numFrames =90
}

local sequences_runningNerd = {
{
name = "normalRun",
frames={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,37,38,39,49,50,51,52,53,54,55,61,62,63,64,65,66,67,68,73,74,75,76,77,78,79,80,81,82,83,84,85,8686,87,88,89,90,91,92,93,98,99,100,101,102,103,104,105,106},
count = 90,
time =6100,
loopCount = 0,
loopDirection = "forward"
}
}



local sheet_runningNerd = graphics.newImageSheet("gufi.png",sheetOptions )
local runningNerd = display.newSprite(sheet_runningNerd,sequences_runningNerd,400,400)







runningNerd.x=display.contentCenterX
runningNerd.y=display.contentCenterY+110




























 -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function cambiaScena ()
composer.gotoScene( "story1" ) --si possono aggiungere effetti
runningNerd.isVisible=false
end





-- create()

function scene:create( event )

local sceneGroup = self.view
-- Code here runs when the scene is first created but has not yet appeared on screen
background = display.newImageRect( sceneGroup, "grunge2.jpg", 750, 400 ) --1024x600
background.x = display.contentCenterX
background.y = display.contentCenterY
sceneGroup:insert(background)

chapter=display.newImageRect(sceneGroup,"chapter2.png",330,200)
chapter.x=display.contentCenterX
chapter.y=display.contentCenterY-70
sceneGroup:insert(chapter)





timer.performWithDelay(0,primofumetto)
timer.performWithDelay(2000,spriteListener3)
runningNerd:play()
end


-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
chapter:addEventListener("tap",cambiaScena)

--Code here runs when the scene is still off screen (but is about to come on screen)
elseif ( phase == "did" ) then

-- Code here runs when the scene is entirely on screen
end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
