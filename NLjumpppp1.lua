    
local hero
local physics=require("physics")
physics.start()
local abutton
isSaltato = 0 -- serve per capire se ha saltato

local function jumpButton(event)
if (event.phase == "began" and isSaltato==1) then 
hero:applyLinearImpulse(0,-0.25, hero.x,hero.y)
isSaltato=0 -- blocco saltox
elseif (event.phase == "ended" ) then 
--hero:setLinearVelocity(80, 0)
end
end 


local function atteramento( self,event) -- self eroe, event terreno
if (event.phase == "began") and (event.other.myName== "heroj") then
isSaltato= 1--attivo salto durante salto
end
end



local function movimentoEroe( event ) 
hero:applyLinearImpulse(0.04,0, hero.x,hero.y)
end

hero = display.newImageRect(mainGroup, "hero.png", 50, 80)
hero.x = display.contentCenterX-200
hero.y = display.contentHeight-60
hero.myName = "heroj"
terreno.collision= atteramento
terreno:addEventListener("collision")

--creo pulsanti per salto
abutton = display.newCircle(150,100,18)
abutton.x = display.contentCenterX+80
abutton.y = display.contentHeight-20
abutton:setFillColor( 0.6, 0.8, 0.4 )
abutton.id = "jump"
abutton:addEventListener("touch", jumpButton )

physics.addBody(terreno, "static", { bounce=0})
physics.addBody(hero, "dynamic", { bounce=0,friction=1})


--creo un timer
timer.performWithDelay(200,movimentoEroe, -1) -- -1 loop infinito, chiamo movimentoEroe metodo

physics.start()
    physics.setGravity(0,9.8)


scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )

return scene




