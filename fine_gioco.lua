
local composer = require( "composer" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local gameOver
local timerMenu




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------


local function cambiaScena ()
 
  composer.gotoScene( "menu") --si possono aggiungere effetti
  
end

-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
   schermataRetry = display.newImageRect(sceneGroup,"sfondo_nero.png", display.contentWidth, display.contentHeight )
   schermataRetry.x = display.contentCenterX
   schermataRetry.y = display.contentCenterY
   gameOver = display.newText(sceneGroup, "THANK YOU FOR PLAYING!",  display.contentCenterX, display.contentHeight*0.6-100, "plasdrip.ttf", 40)
   gameOver:setFillColor( 230/255, 117/255, 20/255 )
   timerMenu = timer.performWithDelay(4000, cambiaScena,  1)  




end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase
  


	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
     


	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
     if timerMenu ~=  nil then
      timer.cancel(timerMenu)
      timerMenu = nil
    end

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
     
           composer.removeScene( "scena_sconfitta_definitiva" )


	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
