local composer = require( "composer" )

local scene = composer.newScene()

audio.reserveChannels(4)

--local preference=require "preference"

--local movieclip=require('movieclip')


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili

local background1
local title
local playBtn
local creditsBtn
local titleView
local t
local currentWorms=0
local wormsHit=0
local totalWors=10
local backscore
local menuButton
local hole1
local hole2
local hole3
local hole4
local hole5
local hole6
local hole7
local tombina


local all=0

local worms


local creditsView

local redscore=0

local timerSource
--local redtext=display.newText(redscore,display.contentCenterX,display.contentCenterY,native.systemFont,128)
local w1
local w2
local w3
local w4
local w5
local w6
local w7
local w8
local worm
local lastWorm={}
local alltalpe=0
local alltalpetotali=50
local redtext

--local redtext=display.newText(redscore,display.contentCenterX,display.contentCenterY,native.systemFont,128)
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni
local function movimentoDX(event)

personaggio:setLinearVelocity( 100, 0 )

end

local function cambiaScena ()
menuButton.isVisible=false
redscore=0
composer.gotoScene( "menu" ) --si possono aggiungere effetti

end

local function startTimer()
timerSource=time.performWithDelay(1400,0)
end


local function endGame()
composer.setVariable("finalScore",redscore)
composer.gotoScene("highscores",{time=800,effect="crossFade"})
end

--local function avviamusica()
--audio.play(backsound,{channel=2,loops=-1})
--end


local function sparisci(self,event)
w1.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end

local function sparisci2(self,event)
w2.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end

local function popola(event)
local randomHole=math.floor(math.random()*7+1)
t[randomHole].isVisible=true
end

local function nascondi(event)
local randomHole=math.floor(math.random()*7+1)
t[randomHole].isVisible=false
end

local function sparisci3(self,event)
w3.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end

local function sparisci4(self,event)
w4.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end


local function sparisci5(self,event)
w5.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end


local function sparisci6(self,event)
w6.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end


local function gotoMenu()
composer.gotoScene( "menu" )
end


local function sparisci7(self,event)
w7.isVisible=false
redscore=redscore+1
redtext.text=tostring(redscore)
end


local function meth()

menuButton.isVisible=true

end


-- create()
function scene:create( event )
local sceneGroup=self.view
physics.pause()
--display.setDefault("background",0,0,255)

backscore=display.newImageRect(500,500)
--sceneGroup:insert(backscore)
-- Code here runs when the scene is first created but has not yet appeared on screen
background1 = display.newImageRect(sceneGroup,"grund2.png",800,600)
background1.x = display.contentCenterX-15
background1.y = display.contentCenterY
sceneGroup:insert(background1)
redtext=display.newText(sceneGroup,redscore,display.contentCenterX+310,display.contentCenterY-165,native.systemFont,78)
sceneGroup:insert(redtext)
--redtext.isVisible=false
w1 = display.newImageRect('zombie2.png', 180, 130)
w1.x=display.contentCenterX-32
w1.y=display.contentCenterY-25
sceneGroup:insert(w1)
w2 = display.newImageRect('zombie2.png', 180, 130)
w2.x=display.contentCenterX+225
w2.y=display.contentCenterY-90
sceneGroup:insert(w2)
w3 = display.newImageRect('zombie2.png', 180, 130)
w3.x=display.contentCenterX+230
w3.y=display.contentCenterY+160
sceneGroup:insert(w3)
w4 = display.newImageRect('zombie2.png', 180, 130)
w4.x=display.contentCenterX+215
w4.y=display.contentCenterY+30
sceneGroup:insert(w4)
w5 = display.newImageRect('zombie2.png', 180, 130)
w5.x=display.contentCenterX-235
w5.y=display.contentCenterY-100
sceneGroup:insert(w5)
w6 = display.newImageRect('zombie2.png', 180, 130)
w6.x=display.contentCenterX-130
w6.y=display.contentCenterY+80
sceneGroup:insert(w6)
w7 = display.newImageRect('zombie2.png', 180, 130)
w7.x=display.contentCenterX-20
w7.y=display.contentCenterY+150
sceneGroup:insert(w7)

--hole1=display.newImageRect('hole.png',200,200)
--hole1.x=display.contentCenterX
--hole1.y=display.contentCenterY



w1:addEventListener("tap",sparisci)
w1.isVisible=false
w2:addEventListener("tap",sparisci2)
w2.isVisible=false
w3:addEventListener("tap",sparisci3)
w3.isVisible=false
w4:addEventListener("tap",sparisci4)
w4.isVisible=false
w5:addEventListener("tap",sparisci5)
w5.isVisible=false
w6:addEventListener("tap",sparisci6)
w6.isVisible=false
w7:addEventListener("tap",sparisci7)
w7.isVisible=false




t={w1,w2,w3,w4,w5,w6,w7,w8}
for i=1,#t do
t[i].isVisible=false


--backsound=audio.loadStream("bird.wav")


menuButton=display.newImageRect(sceneGroup,"GameOver.png",850,600)
menuButton.x=display.contentCenterX
menuButton.y=display.contentCenterY
menuButton.isVisible=false
menuButton:addEventListener("tap",cambiaScena)
sceneGroup:insert(menuButton)

tombina=display.newImageRect(sceneGroup,"bordo.png",770,450)
tombina.x=display.contentCenterX
tombina.y=display.contentCenterY
local hit=audio.loadSound('zzz.mp3')
audio.play(hit)

--composer.setVariabile("finalscore",redscore)
end
end
--local randomHole=math.floor(math.random()*7+1)
--t[randomHole].isVisible=false




--



-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase
--avviamusica()
local allWorms=0
local int tempo=500
if ( phase == "will" ) then
while allWorms<50
do
timer.performWithDelay(tempo,popola)
--timer.performWithDelay(1,sparisci1)
tempo=tempo+500
allWorms=allWorms+1
all=all+1
end
local tempo2=500
local allWorms2=0
while allWorms2<99
do
timer.performWithDelay(tempo2,nascondi)
--timer.performWithDelay(1,sparisci1)
tempo2=tempo2+500
allWorms2=allWorms2+1
end
timer.performWithDelay(tempo+700,meth)

--popola()
--    physics.start()
--local hit=audio.loadSound('cry.wav')

local hit=audio.loadSound('zzz.mp3')
audio.play(hit)
-- Code here runs when the scene is still off screen (but is about to come on screen)


elseif ( phase == "did" ) then

-- Code here runs when the scene is entirely on screen
--physics.start()
--local hit=audio.loadSound('cry.wav')

local hit=audio.loadSound('zzz.mp3')
audio.play(hit)


end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
composer.removeScene( "catch" )
--audio.stop()

end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view
--composer.removeScene( "catch" )
redscore=0
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
