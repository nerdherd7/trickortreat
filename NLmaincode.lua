local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
physics.start()
physics.setGravity( 0, 0 )

math.randomseed( os.time() )

local sheetOptions =
{
    frames =
    {
        {   -- 1) ghost 1
            x = 0,
            y = 0,
            width = 80,
            height = 65
        },
        {   -- 2) ghost 2
            x = 0,
            y = 85,
            width = 60,
            height = 52
        },
        {   -- 3) ghost 3
            x = 0,
            y = 168,
            width = 40,
            height = 38
        },
        {   -- 4) zucca
            x = 0,
            y = 265,
            width = 98,
            height = 79
        },
        {   -- 5) laser
            x = 98,
            y = 265,
            width = 14,
            height = 40
        },
    },
}


local objectSheet = graphics.newImageSheet( "NL11gameObjects.png", sheetOptions )


-- Initialize variables
local lives = 3
local score = 0
local died = false

local ghostsTable = {}

local zucca
local gameLoopTimer
local livesText
local scoreText



--music
local soundEffect = audio.loadSound( "NL111Help.mp3" )
audio.play( soundEffect )


-- Set up display groups
local backGroup = display.newGroup()  -- Display group for the background image
local mainGroup = display.newGroup()  -- Display group for the pumpkin, ghosts, lasers, etc.
local uiGroup = display.newGroup()    -- Display group for UI objects like the score


local background = display.newImageRect( backGroup, "NL11background.png", 800, 1000 )
background.x = display.contentCenterX
background.y = display.contentCenterY


zucca = display.newImageRect( mainGroup, "NL11zucca.png", 60, 55 )
zucca.x = display.contentCenterX
zucca.y = display.contentCenterY+120 
physics.addBody( zucca, { radius=30, isSensor=true } )
zucca.myName = "zucca"



livesText = display.newText( uiGroup, "Lives: " .. lives, 500, 320, native.systemFont, 36 )
scoreText = display.newText( uiGroup, "Score: " .. score, 670, 320, native.systemFont, 36 )

display.setStatusBar( display.HiddenStatusBar )


local function createGhost()

local newGhost = display.newImageRect( mainGroup, "LNGhost.png", 80, 65 ) 
table.insert( ghostsTable, newGhost )
physics.addBody( newGhost, "dynamic", { radius=40, bounce=0.8 } )
newGhost.myName = "ghost"

local whereFrom = math.random( 3 )

if ( whereFrom == 1 ) then

-- From the left
newGhost.x = -60
newGhost.y = math.random( 500 )
newGhost:setLinearVelocity( math.random( 40,120 ), math.random( 20,60 ) )

elseif ( whereFrom == 2 ) then
		-- From the top
		newGhost.x = math.random( display.contentWidth )
		newGhost.y = -60
		newGhost:setLinearVelocity( math.random( -40,40 ), math.random( 40,120 ) )

elseif ( whereFrom == 3 ) then
-- From the right
newGhost.x = display.contentWidth + 60
newGhost.y = math.random( 500 )
newGhost:setLinearVelocity( math.random( -120,-40 ), math.random( 20,60 ) )
end

newGhost:applyTorque( math.random( -6,6 ) )
end


local function fireLaser()

local newLaser = display.newImageRect( mainGroup, "NL11laser.png", 14, 40 )
physics.addBody( newLaser, "dynamic", { isSensor=true } )
newLaser.isBullet = true
newLaser.myName = "laser"

newLaser.x = zucca.x
newLaser.y = zucca.y
newLaser:toBack()

transition.to( newLaser, { y=-40, time=500,
onComplete = function() display.remove( newLaser ) end
	} )
end

zucca:addEventListener( "tap", fireLaser )

local function pullZucca( event )

local zucca = event.target
local phase = event.phase

if ( "began" == phase ) then
-- Set touch focus on the pumpkin
display.currentStage:setFocus( zucca )


zucca.touchOffsetX = event.x - zucca.x

elseif ( "moved" == phase ) then

-- Move the pumpkin to the new touch position
zucca.x = event.x - zucca.touchOffsetX

elseif ( "ended" == phase or "cancelled" == phase ) then
-- Release touch focus on the pumpkin
display.currentStage:setFocus( nil )
	end

return true  -- Prevents touch propagation to underlying objects
end


zucca:addEventListener( "touch", pullZucca )


local function gameLoop()

	createGhost()

-- Remove ghosts which have drifted off screen
	for i = #ghostsTable, 1, -1 do  
local thisGhost = ghostsTable[i]

if ( thisGhost.x < -100 or
 thisGhost.x > display.contentWidth + 100 or
thisGhost.y < -100 or
 thisGhost.y > display.contentHeight + 100 )

then
display.remove( thisGhost )
table.remove( ghostsTable, i ) 
end
end
end


gameLoopTimer = timer.performWithDelay( 500, gameLoop, 0 )


local function restoreZucca()

	zucca.isBodyActive = false
	zucca.x = display.contentCenterX
	zucca.y = display.contentCenterY+120 

	-- Fade in the pumpkin
	transition.to( zucca, { alpha=1, time=4000,
		onComplete = function()
			zucca.isBodyActive = true
			died = false
		end
	} )
end


local function onCollision( event )

	if ( event.phase == "began" ) then

		local obj1 = event.object1
		local obj2 = event.object2

		if ( ( obj1.myName == "laser" and obj2.myName == "ghost" ) or
			 ( obj1.myName == "ghost" and obj2.myName == "laser" ) )
		then


-- Remove both the laser and ghost
display.remove( obj1 )
display.remove( obj2 )

for i = #ghostsTable, 1, -1 do
if ( ghostsTable[i] == obj1 or ghostsTable[i] == obj2 ) then
		table.remove( ghostsTable, i )
	break
end
end

score = score + 100
scoreText.text = "Score: " .. score


elseif ( ( obj1.myName == "zucca" and obj2.myName == "ghost" ) or
		 ( obj1.myName == "ghost" and obj2.myName == "zucca" ) )
	then
if ( died == false ) then
died = true

-- Update lives
				lives = lives - 1
				livesText.text = "Lives: " .. lives

				if ( lives == 0 ) then
 local background1 = display.newImageRect( backGroup, "NL11backgroundfinal1.png", 800, 1000 )
background1.x = display.contentCenterX
background1.y = display.contentCenterY

					display.remove( zucca )
					print("ciao")
				else
					zucca.alpha = 0
					timer.performWithDelay( 100, restoreZucca )
				end
			end
		end
	end
end

Runtime:addEventListener( "collision", onCollision )


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene

