local composer = require( "composer" )
local video=native.newVideo(display.contentCenterX,display.contentCenterY,850,480)
audio.reserveChannels(1)

local function onScreenTouch (event)
video:pause()
video:removeSelf()
video=nil
im.isHitTestable=false

end



-- Go to the menu screen
audio.stop()
video:load("intro.mp4")
video:play()
composer.gotoScene( "menu" )

--video:addEventListener("tap",onScreenTouch)
im=display.newRect(display.contentCenterX,display.contentCenterY,800,800)
im.alpha=0
im.isHitTestable=true
im:addEventListener("tap",onScreenTouch)
--sceneGroup:insert(im)
