local composer = require( "composer" )
local scene = composer.newScene()

display.setStatusBar(display.HiddenStatusBar)
--audio.reserveChannels(1)


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local playbutton
local playcredit
local backsound
local stopmusic
local newGameButton
local newGameButton2
local newGameButton3
local newGameButton4
local newGameButton5
local appmusic=0
local temporaneo
local nuovo
local fb
local video=native.newVideo(display.contentCenterX,display.contentCenterY,850,480)
local sp
local im
local primazucca1
local secondazucca1
local primazucca2
local secondazucca2
local primazucca3
local secondazucca3
local primazucca4
local secondazucca4
local primazucca7
local primazucca8
local primazucca9
local primazucca10
local secondazucca7
local secondazucca8
local secondazucca9
local secondazucca10
local home
local croce

--system.openURL( "http://www.coronalabs.com" )
--Per aggiungere che apre un browser lasciando app

--local webView = native.newWebView( display.contentCenterX, display.contentCenterY, 320, 480 )
--webView:request( "http://www.coronalabs.com/" )
--Per aprire all'interno dell'app

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function cambiaScena ()
composer.gotoScene( "story2" ) --si possono aggiungere effetti
end

local function cambiaScena8 ()
composer.gotoScene( "Zuccarfight" ) --si possono aggiungere effetti
end

local function cambiaScena9 ()
composer.gotoScene( "NLmaincode" ) --si possono aggiungere effetti
end

local function cambiaScena3 ()
composer.gotoScene("catch")
end

local function cambiaScena2 ()
composer.gotoScene( "giocos" ) --si possono aggiungere effetti
end

local function cambiaScena4 ()

composer.gotoScene("level1")
end

local function onScreenTouch (event)
video:pause()
video:removeSelf()
video=nil
im.isHitTestable=false

end




local function cambiaScena7 ()
composer.gotoScene( "menu" ) --si possono aggiungere effetti
end



local function facebook()
system.openURL( "http://www.facebook.com" )
end

local function cambiaScena5 ()
composer.gotoScene("flydream")
end

--local function cambiaScena5 ()
--composer.gotoScene("ghostrun")
--end


--local function avviamusica()
--audio.play(backsound,{channel=1,loops=-1})
--end


local function pausavideo(event)
video:pause()
end


--video:addEventListener("system",onScreenTouch)

local function fermamusica(event)

if(event.phase=="began") then
appmusic=appmusic+1
if(appmusic%2==1) then
audio.stop()
elseif(appmusic%2==0) then
--avviamusica()

print(appmusic)
end
end
end




-- create()

function scene:create( event )

local sceneGroup = self.view
-- Code here runs when the scene is first created but has not yet appeared on screen
--background = display.newImageRect( sceneGroup, "sfondo.png", 700, 600 ) --1024x600
background = display.newImageRect( sceneGroup, "vector.png", 730, 470 )
--background.x = display.contentCenterX+55
--background.y = display.contentCenterY-65
background.x = display.contentCenterX-3
background.y = display.contentCenterY
sceneGroup:insert(background)
backsound=audio.loadStream("swamp.wav")

--stopsound=display.newImageRect("stopmusic.png",50,50)
--stopsound.x=display.contentCenterX-350
--stopsound.y=display.contentCenterY-190
--sceneGroup:insert(stopsound)


newGameButton = display.newRect(display.contentCenterX-33,display.contentCenterY, 240, 40 )
newGameButton.alpha = 0
newGameButton.isHitTestable = true
sceneGroup:insert(newGameButton)

newGameButton4=display.newRect(display.contentCenterX-60,display.contentCenterY+40, 240, 40 )
newGameButton4.alpha = 0
newGameButton4.isHitTestable = true
sceneGroup:insert(newGameButton4)

newGameButton5=display.newRect(display.contentCenterX-33,display.contentCenterY+80, 240, 40 )
newGameButton5.alpha = 0
newGameButton5.isHitTestable = true
sceneGroup:insert(newGameButton5)


newGameButton3=display.newRect(display.contentCenterX-33,display.contentCenterY-40, 240, 40 )
newGameButton3.alpha = 0
newGameButton3.isHitTestable = true
sceneGroup:insert(newGameButton3)

--newGameButton2 = display.newRect( 280, 410, 235, 40 )
--newGameButton2.alpha = 0
--newGameButton2.isHitTestable = true
--sceneGroup:insert(newGameButton2)



--temporaneo=display.newRect( 180, 310, 135, 80 )
--temporaneo.alpha = 4
--temporaneo.isHitTestable = true
--sceneGroup:insert(temporaneo)

--nuovo=display.newImageRect("new.png",100,100)
--nuovo.x=display.contentCenterX-100
--nuovo.y=display.contentCenterY+100

-- playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
-- playButton:setFillColor( 0, 0, 0 )
-- sceneGroup:insert(playButton)

-- playButton:addEventListener( "tap", cambiaScena )
playcredit=display.newRect(display.contentCenterX,display.contentCenterY,80,120)
playcredit.x=display.contentCenterX+330
playcredit.y=display.contentCenterY+115
playcredit.alpha=0
playcredit.isHitTestable=true
sceneGroup:insert(playcredit)


--fb=display.newImageRect(sceneGroup,"fb.png",110,110)
--fb.x=display.contentCenterX+335
--fb.y=display.contentCenterY-165

--video:load("intro.mp4",system.DocumentsDirectory)
--video:play()
--sceneGroup:insert(fb)


--media.playVideo( "intro.mp4", true, onComplete )
--video:addEventListener("touch",pausavideo)
--video:load("intro.mp4")
--video:play()







--video:load("intro.mp4")
--video:play()
--video:addEventListener("tap",onScreenTouch)
im=display.newRect(display.contentCenterX,display.contentCenterY,800,800)
im.alpha=0
im.isHitTestable=true
im:addEventListener("tap",onScreenTouch)

primazucca1=display.newImageRect("primazucca.png",95,95)
primazucca1.x=display.contentCenterX-340
primazucca1.y=display.contentCenterY-170
sceneGroup:insert(primazucca1)


secondazucca1=display.newImageRect("secondazucca.png",95,95)
secondazucca1.x=display.contentCenterX-340
secondazucca1.y=display.contentCenterY-80
sceneGroup:insert(secondazucca1)

primazucca2=display.newImageRect("primazucca.png",95,95)
primazucca2.x=display.contentCenterX-340
primazucca2.y=display.contentCenterY
sceneGroup:insert(primazucca2)



secondazucca2=display.newImageRect("secondazucca.png",95,95)
secondazucca2.x=display.contentCenterX-340
secondazucca2.y=display.contentCenterY+90
sceneGroup:insert(secondazucca2)



primazucca3=display.newImageRect("primazucca.png",95,95)
primazucca3.x=display.contentCenterX-340
primazucca3.y=display.contentCenterY+180
sceneGroup:insert(primazucca3)








primazucca7=display.newImageRect("primazucca.png",95,95)
primazucca7.x=display.contentCenterX+340
primazucca7.y=display.contentCenterY-170
sceneGroup:insert(primazucca7)



secondazucca7=display.newImageRect("secondazucca.png",95,95)
secondazucca7.x=display.contentCenterX+340
secondazucca7.y=display.contentCenterY-80
sceneGroup:insert(secondazucca7)


primazucca8=display.newImageRect("primazucca.png",95,95)
primazucca8.x=display.contentCenterX+340
primazucca8.y=display.contentCenterY
sceneGroup:insert(primazucca8)


secondazucca8=display.newImageRect("secondazucca.png",95,95)
secondazucca8.x=display.contentCenterX+340
secondazucca8.y=display.contentCenterY+90
sceneGroup:insert(secondazucca8)

primazucca9=display.newImageRect("primazucca.png",95,95)
primazucca9.x=display.contentCenterX+340
primazucca9.y=display.contentCenterY+180
sceneGroup:insert(primazucca9)



home=display.newImageRect("back6.png",150,110)
home.x=display.contentCenterX-20
home.y=display.contentCenterY+173
sceneGroup:insert(home)


home:addEventListener("tap",cambiaScena7)

croce=display.newImageRect("croce.png",55,70)
croce.x=display.contentCenterX-73
croce.y=display.contentCenterY+90
sceneGroup:insert(croce)

end


-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
--avviamusica()
--Code here runs when the scene is still off screen (but is about to come on screen)
elseif ( phase == "did" ) then

newGameButton:addEventListener( "tap", cambiaScena3 )
playcredit:addEventListener("tap",cambiaScena2)
--stopsound:addEventListener("touch",fermamusica)
newGameButton3:addEventListener("tap",cambiaScena5)
newGameButton4:addEventListener("tap",cambiaScena8)
newGameButton5:addEventListener("tap",cambiaScena9)
--newGameButton2:addEventListener("tap",cambiaScena)
--temporaneo:addEventListener("tap",cambiaScena3)
--nuovo:addEventListener("tap",cambiaScena5)
--video:load("intro.mp4",system.DocumentsDirectory)
--video:play()

--fb:addEventListener("tap",facebook)
-- Code here runs when the scene is entirely on screen
home:addEventListener("tap",cambiaScena7)


end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
--audio.stop()
--audio.dispose( backsound )
--backsound = nil
--nuovo.isVisible=false
composer.removeScene("menu")
end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
