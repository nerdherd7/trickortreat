local composer = require( "composer" )
local physics = require( "physics" )
local scene = composer.newScene()
physics.start()
local player
local cloud1
local timerSource

display.setStatusBar(display.HiddenStatusBar)


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local playbutton
local playcredit
local newGameButton
local newGameButton2
local newGameButton3
local appmusic=0
local trick
local die
local bat1
local bat2
local bat3
local bat4
local tomb
local ghost1
local ghost2
local ghost3
local ghost4
local ghost5
local ghost6
local cap1
local owl
local house


local sheetOptions =
{
width = 100,
height = 169,
numFrames = 15
}


local sheetOptions2=
{
width=556,
height=504,
numFrames=10
}

local sheet_runningNerd = graphics.newImageSheet("sheet.png",sheetOptions )

local sheet_runningCat=graphics.newImageSheet("gatto.png",sheetOptions )

local sequences_runningNerd = {
{
name = "normalRun",
start = 1,
count = 15,
time = 800,
loopCount = 0,
loopDirection = "forward"
}
}

local sequences_runningCat = {
{
name = "normalRun",
start = 1,
count = 10,
time = 800,
loopCount = 0,
loopDirection = "forward"
}
}


local runningNerd = display.newSprite( sheet_runningNerd,sequences_runningNerd,1150,1150)
runningNerd.x=display.contentCenterX-210
runningNerd.y=display.contentCenterY+135




local function spriteListener( event )

local thisSprite = event.target  -- "event.target" references the sprite

if ( event.phase == "ended" ) then
thisSprite:setSequence( "fastRun" )  -- switch to "fastRun" sequence
thisSprite:play()  -- play the new sequence
end
end

local function spriteListener2( event )

local thisSprite = event.target  -- "event.target" references the sprite
physics.setGravity(0,0)
thisSprite:play()  -- play the new sequence
thisSprite.x=thisSprite.x+4
thisSprite:setLinearVelocity(50,0)
end


local function spriteListener3( )

cloud1.isVisible=false
--physics.setGravity(0,0)
runningNerd:play()  -- play the new sequence
--runningNerd.x=runningNerd.x+4
runningNerd:setLinearVelocity(50,0)
end




local function primofumetto()
physics.setGravity(0,0)
cloud1.isVisible=true
end







--runningNerd :addEventListener( "sprite",spriteListener2) -- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function cambiaScena ()
composer.gotoScene( "level1" ) --si possono aggiungere effetti
end



local function cambiaScena2 ()


composer.gotoScene( "schermata" )
tomb.isVisible=false
cloud1.isVisisble=false
trick.isVisible=false
die.isVisible=false
bat1.isVisible=false
bat2.isVisible=false
bat3.isVisible=false
bat4.isVisible=false
owl.isVisible=false
ghost2.isVisible=false
ghost3.isVisible=false
ghost4.isVisible=false
ghost5.isVisible=false
ghost6.isVisible=false
cap1.isVisible=false
house.isVisible=false
runningNerd:pause()
runningNerd.isVisible=false
 --si possono aggiungere effetti








end



local function onCollision( event )
--physics.setGravity(0,0)
if ( event.phase == "began" ) then

runningNerd:pause()
runningNerd:setLinearVelocity(0,0)
tomb.isVisible=false
cloud1.isVisisble=false
trick.isVisible=false
die.isVisible=false
bat1.isVisible=false
bat2.isVisible=false
bat3.isVisible=false
bat4.isVisible=false
owl.isVisible=false
ghost2.isVisible=true
ghost3.isVisible=true
ghost4.isVisible=true
ghost5.isVisible=true
ghost6.isVisible=true
cap1.isVisible=true
house.isVisible=false
elseif ( event.phase == "ended" ) then
end
end



-- create()

function scene:create( event )

local sceneGroup = self.view
-- Code here runs when the scene is first created but has not yet appeared on screen
background = display.newImageRect( sceneGroup, "sfondostoria.jpg", 800, 600 ) --1024x600
background.x = display.contentCenterX-15
background.y = display.contentCenterY-65
sceneGroup:insert(background)




newGameButton = display.newRect( 280, 410, 235, 40 )
newGameButton.alpha = 0
newGameButton.isHitTestable = true
sceneGroup:insert(newGameButton)

newGameButton3=display.newRect(300,460,300,40)
newGameButton3.alpha = 0
newGameButton3.isHitTestable = true
sceneGroup:insert(newGameButton3)

newGameButton2 = display.newRect( 280, 410, 235, 40 )
newGameButton2.alpha = 0
newGameButton2.isHitTestable = true
sceneGroup:insert(newGameButton2)

-- playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
-- playButton:setFillColor( 0, 0, 0 )
-- sceneGroup:insert(playButton)

-- playButton:addEventListener( "tap", cambiaScena )
playcredit=display.newRect(display.contentCenterX,display.contentCenterY,80,120)
playcredit.x=display.contentCenterX+330
playcredit.y=display.contentCenterY+115
playcredit.alpha=0
playcredit.isHitTestable=true

cloud1=display.newImageRect("cloud.png",300,250)
cloud1.x=display.contentCenterX-100
cloud1.y=display.contentCenterY+80
cloud1.isVisible=false


sceneGroup:insert(playcredit)
physics.setGravity(0,0)
trick=display.newImageRect("trick.png",100,100)
trick.x=display.contentCenterX+100
trick.y=display.contentCenterY+180
physics.addBody(trick,"static")
physics.addBody(runningNerd,"dynamic")

die=display.newImageRect("die.jpg",200,200)
die.x=display.contentCenterX+270
die.y=display.contentCenterY-100

bat1=display.newImageRect("bat.png",100,80)
bat1.x=display.contentCenterX-30
bat1.y=display.contentCenterY-100

bat2=display.newImageRect("bat.png",100,80)
bat2.x=display.contentCenterX
bat2.y=display.contentCenterY



bat3=display.newImageRect("bat.png",100,80)
bat3.x=display.contentCenterX+140
bat3.y=display.contentCenterY-50

bat4=display.newImageRect("bat.png",100,80)
bat4.x=display.contentCenterX+80
bat4.y=display.contentCenterY-92



tomb=display.newImageRect("tomb.png",300,450)
tomb.x=display.contentCenterX+280
tomb.y=display.contentCenterY+200



ghost2=display.newImageRect("grunge.jpg",1200,1350)
ghost2.x=display.contentCenterX-250
ghost2.y=display.contentCenterY+100
ghost2.isVisible=false


ghost3=display.newImageRect("ghost3.jpg",260,550)
ghost3.x=display.contentCenterX+210
ghost3.y=display.contentCenterY+80
ghost3.isVisible=false

ghost4=display.newImageRect("ghost4.jpeg",260,550)
ghost4.x=display.contentCenterX+800
ghost4.y=display.contentCenterY-250
ghost4.isVisible=false

ghost5=display.newImageRect("ghost5.png",260,550)
ghost5.x=display.contentCenterX+800
ghost5.y=display.contentCenterY+200
ghost5.isVisible=false

ghost6=display.newImageRect("ghost6.jpeg",260,550)
ghost6.x=display.contentCenterX+800
ghost6.y=display.contentCenterY+100
ghost6.isVisible=false

cap1=display.newImageRect("cap.jpg",350,250)
cap1.x=display.contentCenterX
cap1.y=display.contentCenterY-110
cap1.isVisible=false

owl=display.newImageRect("owl.png",350,250)
owl.x=display.contentCenterX-250
owl.y=display.contentCenterY-110
--cap1.isVisible=false


house=display.newImageRect("house.jpg",220,220)
house.x=display.contentCenterX-380
house.y=display.contentCenterY+150

--runningNerd :addEventListener( "tap",primofumetto)
timer.performWithDelay(0,primofumetto)
timer.performWithDelay(2000,spriteListener3)
runningNerd :addEventListener("collision",cambiaScena2)
end


-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then


--Code here runs when the scene is still off screen (but is about to come on screen)
elseif ( phase == "did" ) then

-- Code here runs when the scene is entirely on screen
end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
--composer.removeScene("story1")
end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view
composer.removeScene(story1)

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
