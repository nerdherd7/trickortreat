--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:cc6c3b72751e32c604a323becf81f260:8a429fa07137f3feeaa1bb1b5a05e062:9268187d228515104b4214ad4ea4f19e$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Idle (1)
            x=579,
            y=917,
            width=284,
            height=461,

            sourceX = 72,
            sourceY = 47,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (2)
            x=1,
            y=915,
            width=286,
            height=459,

            sourceX = 72,
            sourceY = 49,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (3)
            x=289,
            y=917,
            width=288,
            height=459,

            sourceX = 72,
            sourceY = 49,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (4)
            x=597,
            y=458,
            width=290,
            height=457,

            sourceX = 71,
            sourceY = 51,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (5)
            x=601,
            y=1,
            width=292,
            height=455,

            sourceX = 71,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (6)
            x=895,
            y=1,
            width=292,
            height=455,

            sourceX = 72,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (7)
            x=1189,
            y=1,
            width=294,
            height=455,

            sourceX = 72,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (8)
            x=1485,
            y=1,
            width=296,
            height=455,

            sourceX = 71,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (9)
            x=1,
            y=458,
            width=298,
            height=455,

            sourceX = 71,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (10)
            x=1,
            y=1,
            width=300,
            height=455,

            sourceX = 71,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (11)
            x=303,
            y=1,
            width=296,
            height=455,

            sourceX = 72,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (12)
            x=301,
            y=458,
            width=294,
            height=457,

            sourceX = 72,
            sourceY = 51,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (13)
            x=889,
            y=458,
            width=292,
            height=459,

            sourceX = 72,
            sourceY = 49,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (14)
            x=1183,
            y=458,
            width=290,
            height=459,

            sourceX = 71,
            sourceY = 49,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Idle (15)
            x=1475,
            y=458,
            width=288,
            height=459,

            sourceX = 71,
            sourceY = 49,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (1)
            x=865,
            y=919,
            width=284,
            height=461,

            sourceX = 72,
            sourceY = 47,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (2)
            x=663,
            y=1382,
            width=316,
            height=475,

            sourceX = 38,
            sourceY = 44,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (3)
            x=981,
            y=1382,
            width=336,
            height=475,

            sourceX = 16,
            sourceY = 41,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (4)
            x=325,
            y=1380,
            width=336,
            height=469,

            sourceX = 16,
            sourceY = 41,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (5)
            x=1,
            y=1378,
            width=322,
            height=467,

            sourceX = 32,
            sourceY = 44,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (6)
            x=1151,
            y=919,
            width=312,
            height=461,

            sourceX = 44,
            sourceY = 47,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (7)
            x=1765,
            y=458,
            width=274,
            height=477,

            sourceX = 78,
            sourceY = 42,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (8)
            x=1319,
            y=1390,
            width=280,
            height=479,

            sourceX = 68,
            sourceY = 39,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (9)
            x=1751,
            y=937,
            width=288,
            height=473,

            sourceX = 60,
            sourceY = 39,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Walk (10)
            x=1465,
            y=919,
            width=284,
            height=469,

            sourceX = 68,
            sourceY = 43,
            sourceWidth = 430,
            sourceHeight = 519
        },
    },
    
    sheetContentWidth = 2040,
    sheetContentHeight = 1870
}

SheetInfo.frameIndex =
{

    ["Idle (1)"] = 1,
    ["Idle (2)"] = 2,
    ["Idle (3)"] = 3,
    ["Idle (4)"] = 4,
    ["Idle (5)"] = 5,
    ["Idle (6)"] = 6,
    ["Idle (7)"] = 7,
    ["Idle (8)"] = 8,
    ["Idle (9)"] = 9,
    ["Idle (10)"] = 10,
    ["Idle (11)"] = 11,
    ["Idle (12)"] = 12,
    ["Idle (13)"] = 13,
    ["Idle (14)"] = 14,
    ["Idle (15)"] = 15,
    ["Walk (1)"] = 16,
    ["Walk (2)"] = 17,
    ["Walk (3)"] = 18,
    ["Walk (4)"] = 19,
    ["Walk (5)"] = 20,
    ["Walk (6)"] = 21,
    ["Walk (7)"] = 22,
    ["Walk (8)"] = 23,
    ["Walk (9)"] = 24,
    ["Walk (10)"] = 25,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
