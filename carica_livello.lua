
local composer = require( "composer" )
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local timerLivello
local progresso

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function cambiaScena ()
  if progresso == 1 then
      composer.gotoScene( "level1") --si possono aggiungere effetti
  elseif progresso == 2 then
      composer.gotoScene( "level2")
  elseif progresso == 3 then
      composer.gotoScene( "level3")
  elseif progresso == 4 then
      composer.gotoScene( "fine_gioco")
  end
    
end


local function loadProgresso()
  progresso =  composer.getVariable("final_progresso")
end

loadProgresso()
if progresso == nil then
  
  print("ERRORE CARICAMENTO PROGRESSO!")
  
end


-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
  timerLivello = timer.performWithDelay( 100, cambiaScena,1)
  

end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase
  


	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
   -- pulsanteRiprova:addEventListener("tap",tapListener)--ricomincia il gioco

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen


    


	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
 

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
       
           timer.cancel(timerLivello)
           timerLivello = nil
           composer.removeScene( "carica_livello" )


	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
