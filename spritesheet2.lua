--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:c6db1949ef2ed26e5b351d98ea7f1e21:0e71e37c90bbbf95fc0b82c44abd77a5:6fa81e56229cdcc920d2f332f648e35d$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Slide (1)
            x=983,
            y=1,
            width=511,
            height=538,

            sourceX = 0,
            sourceY = 24,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (2)
            x=1492,
            y=545,
            width=507,
            height=540,

            sourceX = 0,
            sourceY = 22,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (3)
            x=1496,
            y=1,
            width=501,
            height=542,

            sourceX = 0,
            sourceY = 20,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (4)
            x=494,
            y=1083,
            width=497,
            height=544,

            sourceX = 0,
            sourceY = 18,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (5)
            x=1,
            y=551,
            width=491,
            height=546,

            sourceX = 0,
            sourceY = 16,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (6)
            x=1,
            y=1,
            width=487,
            height=548,

            sourceX = 0,
            sourceY = 14,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (7)
            x=490,
            y=1,
            width=491,
            height=546,

            sourceX = 0,
            sourceY = 16,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (8)
            x=993,
            y=1083,
            width=497,
            height=544,

            sourceX = 0,
            sourceY = 18,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (9)
            x=1492,
            y=1087,
            width=501,
            height=542,

            sourceX = 0,
            sourceY = 20,
            sourceWidth = 519,
            sourceHeight = 562
        },
        {
            -- Slide (10)
            x=983,
            y=541,
            width=507,
            height=540,

            sourceX = 0,
            sourceY = 22,
            sourceWidth = 519,
            sourceHeight = 562
        },
    },
    
    sheetContentWidth = 2000,
    sheetContentHeight = 1630
}

SheetInfo.frameIndex =
{

    ["Slide (1)"] = 1,
    ["Slide (2)"] = 2,
    ["Slide (3)"] = 3,
    ["Slide (4)"] = 4,
    ["Slide (5)"] = 5,
    ["Slide (6)"] = 6,
    ["Slide (7)"] = 7,
    ["Slide (8)"] = 8,
    ["Slide (9)"] = 9,
    ["Slide (10)"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
