--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:7cce3f14cca01264b0c988f98a65c31a:14f769ff109cd10092a1201b6b507bef:e2780797996697b268e4a0eb6158fd52$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Attack (1)
            x=305,
            y=1084,
            width=270,
            height=457,

            sourceX = 72,
            sourceY = 51,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (2)
            x=1727,
            y=740,
            width=276,
            height=455,

            sourceX = 28,
            sourceY = 53,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (3)
            x=1455,
            y=740,
            width=270,
            height=451,

            sourceX = 58,
            sourceY = 57,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (4)
            x=386,
            y=651,
            width=348,
            height=431,

            sourceX = 71,
            sourceY = 78,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (5)
            x=1119,
            y=677,
            width=334,
            height=441,

            sourceX = 71,
            sourceY = 69,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (6)
            x=1712,
            y=289,
            width=318,
            height=449,

            sourceX = 72,
            sourceY = 61,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (7)
            x=1,
            y=1067,
            width=302,
            height=455,

            sourceX = 72,
            sourceY = 55,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Attack (8)
            x=988,
            y=1120,
            width=288,
            height=459,

            sourceX = 71,
            sourceY = 51,
            sourceWidth = 430,
            sourceHeight = 519
        },
        {
            -- Dead (1)
            x=1,
            y=641,
            width=383,
            height=424,

            sourceX = 0,
            sourceY = 42,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (2)
            x=577,
            y=1095,
            width=409,
            height=458,

            sourceX = 19,
            sourceY = 32,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (3)
            x=736,
            y=661,
            width=381,
            height=432,

            sourceX = 64,
            sourceY = 33,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (4)
            x=1363,
            y=289,
            width=347,
            height=386,

            sourceX = 62,
            sourceY = 71,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (5)
            x=1066,
            y=281,
            width=295,
            height=378,

            sourceX = 89,
            sourceY = 91,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (6)
            x=751,
            y=281,
            width=313,
            height=374,

            sourceX = 87,
            sourceY = 94,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (7)
            x=424,
            y=279,
            width=325,
            height=370,

            sourceX = 88,
            sourceY = 97,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (8)
            x=1,
            y=279,
            width=421,
            height=360,

            sourceX = 91,
            sourceY = 114,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (9)
            x=1498,
            y=1,
            width=493,
            height=286,

            sourceX = 94,
            sourceY = 186,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (10)
            x=999,
            y=1,
            width=497,
            height=278,

            sourceX = 93,
            sourceY = 192,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (11)
            x=1,
            y=1,
            width=497,
            height=276,

            sourceX = 94,
            sourceY = 197,
            sourceWidth = 629,
            sourceHeight = 526
        },
        {
            -- Dead (12)
            x=500,
            y=1,
            width=497,
            height=276,

            sourceX = 94,
            sourceY = 198,
            sourceWidth = 629,
            sourceHeight = 526
        },
    },
    
    sheetContentWidth = 2031,
    sheetContentHeight = 1580
}

SheetInfo.frameIndex =
{

    ["Attack (1)"] = 1,
    ["Attack (2)"] = 2,
    ["Attack (3)"] = 3,
    ["Attack (4)"] = 4,
    ["Attack (5)"] = 5,
    ["Attack (6)"] = 6,
    ["Attack (7)"] = 7,
    ["Attack (8)"] = 8,
    ["Dead (1)"] = 9,
    ["Dead (2)"] = 10,
    ["Dead (3)"] = 11,
    ["Dead (4)"] = 12,
    ["Dead (5)"] = 13,
    ["Dead (6)"] = 14,
    ["Dead (7)"] = 15,
    ["Dead (8)"] = 16,
    ["Dead (9)"] = 17,
    ["Dead (10)"] = 18,
    ["Dead (11)"] = 19,
    ["Dead (12)"] = 20,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
