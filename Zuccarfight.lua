local composer = require( "composer" )
local scene = composer.newScene()
local physics=require("physics")
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
-- VARIABILI GRAFICHE-------------------------------------------------

local zucca
local raggio
local pavimento
local background
local murodx,murosx,muroup
local potenza 
--Statistiche boss----------------------------------------
local barrahpvuota
local barrabosshp
local boss
local hpboss=500
local hpbarra=10
local verso
local tempoy=0
local x=0
local blocco=1
local vita={}
local i=0
local immortalepersonaggio=0
local immortalemostro=0
--Attacchi Boss---------------------------------------------
local triplesfere={}
--statis giocatore---------------------------
local hpgiocatore=200
local danno=25
--ALTRE VARIABILI------------------------------------
local physics=require("physics");
--FUNZIONI----------------------------------
--------------------------------------------------------
local function collisionesfera3(self,event)
if ( event.phase == "began" ) then
if( event.other.myName=="terreno") then 
display.remove(triplesfere[2])
end
end
end
local function collisionesfera2(self,event)
if ( event.phase == "began" ) then
if( event.other.myName=="terreno") then 
display.remove(triplesfere[1])
end
end
end
local function collisionesfera1(self,event)
if ( event.phase == "began" ) then
if( event.other.myName=="terreno") then 
display.remove(triplesfere[0])
end
end
end
-----------------------------------------------------------------
local function sistemabattle(scelta)
if(scelta==0) then
triplesfere[0]:setFillColor( 255, 0, 0 )
triplesfere[1]:setFillColor( 255, 0, 0 )
triplesfere[2]:setFillColor( 255, 0, 0 )
triplesfere[0].myName = "triplesfere0"
triplesfere[1].myName = "triplesfere1"
triplesfere[2].myName = "triplesfere2"
physics.addBody(triplesfere[0],"dynamic")
physics.addBody(triplesfere[1],"dynamic")
physics.addBody(triplesfere[2],"dynamic")
triplesfere[0].collision=collisionesfera1
triplesfere[0]:addEventListener("collision")
triplesfere[1].collision=collisionesfera2
triplesfere[1]:addEventListener("collision")
triplesfere[2].collision=collisionesfera3
triplesfere[2]:addEventListener("collision")
elseif(scelta==1) then
triplesfere[0]:setFillColor( 255, 0, 0 )
triplesfere[0].myName = "triplesfere0"
physics.addBody(triplesfere[0],"dynamic")
end
end
-------------------------------------------------------
local function ability(abilita)
if(abilita==0) then
triplesfere[0]=display.newCircle(boss.x,boss.y+30,5)
triplesfere[1]=display.newCircle(boss.x+20,boss.y+30,5)
triplesfere[2]=display.newCircle(boss.x+40,boss.y+30,5)
sistemabattle(0)
elseif(abilita==1) then
triplesfere[0]=display.newCircle(boss.x,boss.y+30,5)
sistemabattle(1)
end
end
------------------------------------------------------------------
local function actionboss()
skills=math.random(0, 1)

if(skills==0) then
ability(0)
if( triplesfere[0]~=nil) then		
triplesfere[0]:setLinearVelocity(-300,100 ,triplesfere[0].x,triplesfere[0].y)
end
if( triplesfere[2]~=nil) then
triplesfere[2]:setLinearVelocity(300,100 ,triplesfere[2].x,triplesfere[2].y)
end
elseif(skills==1) then
ability(1)
elseif(skills==2) then
print("C")
elseif(skills==3) then
print("D")
end
end
----------------------------------------------------------------
local function gameover()
if(i==4) then
print("ciaoooooooooooooooooooo")
composer.gotoScene("Gameover",{effect="fade" ,time=5000})
physics.stop()
end
end
------------------------------------------------------------------
local function loselife()
display.remove(vita[i])
i=i+1
immortalepersonaggio=0
gameover()
end
-----------------------------------------------------------------------------------
local function statszucca(self,event)
if ( event.phase == "began" ) then
if( event.other.myName=="triplesfere0" and immortalepersonaggio>80) then 
loselife()
display.remove(triplesfere[0])
elseif( event.other.myName=="triplesfere1" and immortalepersonaggio>80) then
loselife()
display.remove(triplesfere[1])
 elseif(event.other.myName=="triplesfere2" and immortalepersonaggio>80) then
loselife()
display.remove(triplesfere[2])
end
end
end
-------------------------------------------------------------------------------------
local function movboss(event)
boss.rotation = 0
immortalepersonaggio=immortalepersonaggio+1
 immortalemostro= immortalemostro+1
x=x+1;
blocco=1
tempoy=tempoy+1

if(x>=200 and x<=230 ) then
blocco=0
end

if(x==231) then
actionboss()
x=0
end
--[
if(blocco==1) then
if(tempoy<=100 ) then
print(tempoy)
boss:setLinearVelocity(-300,10  )
end
if(tempoy>=201) then 
tempoy=0
end
--]

if(verso==1 ) then
boss:setLinearVelocity(-100,0)
boss.xScale = -1
else
boss:setLinearVelocity(100,0 )
boss.xScale = 1
end
else 
--fine velocit�
boss:setLinearVelocity(0,math.random(0, 0)  )
end
end
-----------------------------------------------------------------------------------------
local function backgroundmaxmovimento(event)
      if ( event.phase == "began" ) then

        -- Code executed when the button is touched
    elseif ( event.phase == "moved" ) then
physics .start()
raggio.isVisible = false
        -- Code executed when the touch is moved over the object
     
    elseif ( event.phase == "ended" ) then
        -- Code executed when the touch lifts off the object

     
    end
    return true  -- Prevents tap/touch propagation to underlying objects

end
-----------------------------------------------------------------------------------------
local function raggio1(event)
      if ( event.phase == "began" ) then


        -- Code executed when the button is touched
 
    elseif ( event.phase == "moved" ) then

        -- Code executed when the touch is moved over the object
    elseif ( event.phase == "ended" ) then
        -- Code executed when the touch lifts off the object

zucca:applyLinearImpulse((event.x-raggio.x)/350,(event.y-raggio.y)/350 ,zucca.x,zucca.y)

raggio.isVisible = false
physics .start()
       
    end
    return true  -- Prevents tap/touch propagation to underlying objects

end
----------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------

local function myButtonHandler( event )
 
     if ( event.phase == "began" ) then

raggio= display.newCircle(zucca.x,zucca.y,700)
zucca:setLinearVelocity( 0, 0 )
raggio:setStrokeColor( 1, 0, 0 )
raggio.alpha=0.1
pavimento:setFillColor( 1,10, 10 )
raggio:addEventListener("touch",raggio1 )
physics.pause ()
background:addEventListener("touch", backgroundmaxmovimento)
    elseif ( event.phase == "ended" ) then


raggio.isVisibled = false
    end
    return true  -- Prevents tap/touch propagation to underlying objects
end
------------------------------------------------------------------------------------------
local function statsboss(self,event)
        if ( event.phase == "began" ) then

 if( event.other.myName=="murodx") then
verso=1
end

 if( event.other.myName=="murosx") then
verso=0
end

 if( event.other.myName=="zucca" and immortalemostro>30) then
immortalemostro=0
hpboss= hpboss-danno
boss:setFillColor( 255, 0, 0 )
barrabosshp.width= hpboss
if(hpboss<=0) then
physics.pause()
end
end
    elseif ( event.phase == "ended" ) then
        boss:setFillColor( 255, 255, 255,255 )
    end
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------


-- create()
function scene:create()
local sceneGroup = self.view




background= display.newImageRect("BG_scroll.png",800,700)
background.x=display.contentCenterX
background.y=display.contentCenterY


zucca = display.newImageRect("zucca1.png",50,50)
zucca.x=display.contentCenterX
zucca.y=display.contentCenterY
zucca.myName = "zucca"

pavimento = display.newImageRect("terreno.png",800,30 )
pavimento.x=display.contentCenterX
pavimento.y=display.contentCenterY+210
pavimento.myName = "terreno"

murodx = display.newRect(  display.contentCenterX+400, display.contentCenterY,10,1024 )
murodx:setFillColor( 1, 10, 10 )
murodx.myName = "murodx"

murosx = display.newRect(  display.contentCenterX-400, display.contentCenterY,10,1024 )
murosx:setFillColor( 1, 10, 10 )
murosx.myName = "murosx"

muroup = display.newRect(  display.contentCenterX, display.contentCenterY-227,1024,10)
muroup:setFillColor( 1, 10, 10 )


vita[0] = display.newCircle(display.contentCenterX+300,display.contentCenterY-200,10)
vita[0]:setFillColor( 255, 0, 0 )
vita[1] = display.newCircle(display.contentCenterX+330,display.contentCenterY-200,10)
vita[1]:setFillColor( 255, 0, 0 )
vita[2] = display.newCircle(display.contentCenterX+360,display.contentCenterY-200,10)
vita[2]:setFillColor( 255, 0, 0 )

boss = display.newImageRect("fantasmamovdx.png",100,100)
boss.x=display.contentCenterX
boss.y=display.contentCenterY-150
boss.myName = "boss"

zucca:addEventListener("touch", myButtonHandler)

boss.collision=statsboss
boss:addEventListener("collision")

zucca.collision=statszucca
zucca:addEventListener("collision")

backsound=audio.loadStream("hoorr.wav")
audio.play(backsound)

--timer
timer.performWithDelay(1,movboss,-1)
--hp 385
barrabosshp = display.newRect(display.contentCenterX,310,385,20)
barrabosshp :setFillColor( 255, 255, 255 )
	-- Code here runs when the scene is first created but has not yet appeared on screen
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
physics.start()
----------FISICA CENTRALE-----------------------
physics.setGravity(0,9.8)
physics.addBody(zucca,{bounce= 0.7})
physics.addBody(pavimento,"static")
physics.addBody(murodx,"static")
physics.addBody(murosx,"static")
physics.addBody(muroup,"static")

physics.addBody(boss,"kynematic",{friction=1})
boss.gravityScale = 0
-----------------------------------------------------
	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)
	

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
composer.removeScene("Zuccarfight")
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene