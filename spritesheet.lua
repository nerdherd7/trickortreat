--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:8aa8c56ead3ba7b4ad357dbdb8010547:c0f186af39f9486860c2c161375ca7fd:c46243f411ca86fe596665ab352bd775$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- dead
            x=1215,
            y=1165,
            width=516,
            height=384,

        },
        {
            -- idle-1
            x=769,
            y=586,
            width=372,
            height=581,

        },
        {
            -- idle-2
            x=1143,
            y=584,
            width=372,
            height=579,

            sourceX = 0,
            sourceY = 2,
            sourceWidth = 372,
            sourceHeight = 581
        },
        {
            -- idle-3
            x=1505,
            y=1,
            width=372,
            height=577,

            sourceX = 0,
            sourceY = 4,
            sourceWidth = 372,
            sourceHeight = 581
        },
        {
            -- idle-4
            x=1131,
            y=1,
            width=372,
            height=581,

        },
        {
            -- jump
            x=1,
            y=1203,
            width=396,
            height=526,

        },
        {
            -- run-1
            x=393,
            y=592,
            width=374,
            height=587,

            sourceX = 24,
            sourceY = 14,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-2
            x=1,
            y=1,
            width=370,
            height=601,

            sourceX = 26,
            sourceY = 0,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-3
            x=399,
            y=1181,
            width=390,
            height=555,

            sourceX = 15,
            sourceY = 23,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-4
            x=1517,
            y=580,
            width=370,
            height=577,

            sourceX = 25,
            sourceY = 24,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-5
            x=373,
            y=1,
            width=368,
            height=589,

            sourceX = 26,
            sourceY = 12,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-6
            x=1,
            y=604,
            width=390,
            height=597,

            sourceX = 15,
            sourceY = 4,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-7
            x=791,
            y=1169,
            width=422,
            height=567,

            sourceX = 0,
            sourceY = 16,
            sourceWidth = 422,
            sourceHeight = 601
        },
        {
            -- run-8
            x=743,
            y=1,
            width=386,
            height=583,

            sourceX = 18,
            sourceY = 18,
            sourceWidth = 422,
            sourceHeight = 601
        },
    },
    
    sheetContentWidth = 1888,
    sheetContentHeight = 1737
}

SheetInfo.frameIndex =
{

    ["dead"] = 1,
    ["idle-1"] = 2,
    ["idle-2"] = 3,
    ["idle-3"] = 4,
    ["idle-4"] = 5,
    ["jump"] = 6,
    ["run-1"] = 7,
    ["run-2"] = 8,
    ["run-3"] = 9,
    ["run-4"] = 10,
    ["run-5"] = 11,
    ["run-6"] = 12,
    ["run-7"] = 13,
    ["run-8"] = 14,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
