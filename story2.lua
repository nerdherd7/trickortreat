local composer = require( "composer" )
local physics = require( "physics" )
local scene = composer.newScene()
physics.start()
local player
local cloud1
local timerSource

display.setStatusBar(display.HiddenStatusBar)


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local playbutton
local playcredit
local newGameButton
local newGameButton2
local newGameButton3
local appmusic=0
local chapter


local sheetOptions =
{
width = 400,
height = 199,
numFrames = 24
}


local sheetOptions2=
{
width=556,
height=504,
numFrames=10
}



local sheet_runningCat=graphics.newImageSheet("gatto.png",sheetOptions )

local sequences_runningNerd = {
{
name = "normalRun",
start = 1,
count = 24,
time =4200,
loopCount = 0,
loopDirection = "forward"
}
}

local sequences_runningCat = {
{
name = "normalRun",
start = 1,
count = 10,
time = 800,
loopCount = 0,
loopDirection = "forward"
}
}



local sheet_runningNerd = graphics.newImageSheet("catt.png",sheetOptions )




local runningNerd = display.newSprite(sheet_runningNerd,sequences_runningNerd,200,200)





runningNerd.x=display.contentCenterX
runningNerd.y=display.contentCenterY+120











 -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function cambiaScena ()
composer.gotoScene( "story1" ) --si possono aggiungere effetti
runningNerd.isVisible=false
end





-- create()

function scene:create( event )

local sceneGroup = self.view
-- Code here runs when the scene is first created but has not yet appeared on screen
background = display.newImageRect( sceneGroup, "grunge.jpg", 750, 400 ) --1024x600
background.x = display.contentCenterX
background.y = display.contentCenterY
sceneGroup:insert(background)

chapter=display.newImageRect(sceneGroup,"chapter11.png",200,200)
chapter.x=display.contentCenterX
chapter.y=display.contentCenterY-80
sceneGroup:insert(chapter)





timer.performWithDelay(0,primofumetto)
timer.performWithDelay(2000,spriteListener3)
runningNerd:play()
end


-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
chapter:addEventListener("tap",cambiaScena)

--Code here runs when the scene is still off screen (but is about to come on screen)
elseif ( phase == "did" ) then

-- Code here runs when the scene is entirely on screen
end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
