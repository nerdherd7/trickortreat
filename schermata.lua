
local composer = require( "composer" )

local scene = composer.newScene()

local home
local tk


--local function cambiaScena8 ()
--composer.gotoScene( "level1" ) --si possono aggiungere effetti
--end


local function cambiaScena8 ()
composer.gotoScene( "level1" ) --si possono aggiungere effetti
end



local function cambiaScena7 ()
composer.gotoScene( "menu" ) --si possono aggiungere effetti
end

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
--local background
--local cloud=display.newImageRect("cloud.png",165,105)
--cloud.x=display.contentCenterX+20
--cloud.y=display.contentCenterY+20


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

local function fumetti(event)
if(event.phase=="began")
  then cloud.alpha=1
end
end
-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
  background = display.newImageRect("finaleadf.png",768,430 ) --1024x600
  background.x = display.contentCenterX
  background.y = display.contentCenterY
  sceneGroup:insert(background)
  


--cloud.alpha=0
--cloud.isHitTestable=true
--cloud:addEventListener("touch",fumetti)
-- playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
 -- playButton:setFillColor( 0, 0, 0 )
 -- sceneGroup:insert(playButton)

--home=display.newImageRect("finaleadf.png",90,90)
--home.x=display.contentCenterX+346
--home.y=display.contentCenterY-178
--sceneGroup:insert(home)

--home:addEventListener("tap",cambiaScena7)


 -- playButton:addEventListener( "tap", cambiaScena )


tk=display.newRect(display.contentCenterX,display.contentCenterY,330,300)
tk.alpha=0
tk.isHitTestable=true
tk:addEventListener("tap",cambiaScena8)


end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
