
local composer = require( "composer" )
local scene = composer.newScene()
local physics = require( "physics" )
local perspective = require("perspective")
system.activate("multitouch")
local camera = perspective.createView(40)
physics.start()


local sheetOptions2 =
{
  frames = {

    {
      -- Slide (1)
      x=983,
      y=1,
      width=511,
      height=538,

      sourceX = 0,
      sourceY = 24,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (2)
      x=1492,
      y=545,
      width=507,
      height=540,

      sourceX = 0,
      sourceY = 22,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (3)
      x=1496,
      y=1,
      width=501,
      height=542,

      sourceX = 0,
      sourceY = 20,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (4)
      x=494,
      y=1083,
      width=497,
      height=544,

      sourceX = 0,
      sourceY = 18,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (5)
      x=1,
      y=551,
      width=491,
      height=546,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (6)
      x=1,
      y=1,
      width=487,
      height=548,

      sourceX = 0,
      sourceY = 14,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (7)
      x=490,
      y=1,
      width=491,
      height=546,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (8)
      x=993,
      y=1083,
      width=497,
      height=544,

      sourceX = 0,
      sourceY = 18,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (9)
      x=1492,
      y=1087,
      width=501,
      height=542,

      sourceX = 0,
      sourceY = 20,
      sourceWidth = 519,
      sourceHeight = 562
    },
    {
      -- Slide (10)
      x=983,
      y=541,
      width=507,
      height=540,

      sourceX = 0,
      sourceY = 22,
      sourceWidth = 519,
      sourceHeight = 562
    },
  },

  sheetContentWidth = 2000,
  sheetContentHeight = 1630
}

local sheetOptions =
{
  frames = {

    {
      -- dead
      x=1215,
      y=1165,
      width=516,
      height=384,

    },
    {
      -- idle-1
      x=769,
      y=586,
      width=372,
      height=581,

    },
    {
      -- idle-2
      x=1143,
      y=584,
      width=372,
      height=579,

      sourceX = 0,
      sourceY = 2,
      sourceWidth = 372,
      sourceHeight = 581
    },
    {
      -- idle-3
      x=1505,
      y=1,
      width=372,
      height=577,

      sourceX = 0,
      sourceY = 4,
      sourceWidth = 372,
      sourceHeight = 581
    },
    {
      -- idle-4
      x=1131,
      y=1,
      width=372,
      height=581,

    },
    {
      -- jump
      x=1,
      y=1203,
      width=396,
      height=526,

    },
    {
      -- run-1
      x=393,
      y=592,
      width=374,
      height=587,

      sourceX = 24,
      sourceY = 14,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-2
      x=1,
      y=1,
      width=370,
      height=601,

      sourceX = 26,
      sourceY = 0,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-3
      x=399,
      y=1181,
      width=390,
      height=555,

      sourceX = 15,
      sourceY = 23,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-4
      x=1517,
      y=580,
      width=370,
      height=577,

      sourceX = 25,
      sourceY = 24,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-5
      x=373,
      y=1,
      width=368,
      height=589,

      sourceX = 26,
      sourceY = 12,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-6
      x=1,
      y=604,
      width=390,
      height=597,

      sourceX = 15,
      sourceY = 4,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-7
      x=791,
      y=1169,
      width=422,
      height=567,

      sourceX = 0,
      sourceY = 16,
      sourceWidth = 422,
      sourceHeight = 601
    },
    {
      -- run-8
      x=743,
      y=1,
      width=386,
      height=583,

      sourceX = 18,
      sourceY = 18,
      sourceWidth = 422,
      sourceHeight = 601
    },
  },

  sheetContentWidth = 1888,
  sheetContentHeight = 1737
}



-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local eroeSheet1 = graphics.newImageSheet( "spritesheet.png", sheetOptions )
local eroeSheet2 = graphics.newImageSheet( "spritesheet2.png", sheetOptions2 )
local centro_schermo = display.contentCenterX
local eroeJump
local terreno
local burrone
local spike
local background1
local background2
local background3
local background4
local eroe
local zombieM
local zombieM2
local backGroup 
local mainGroup
local uiGroup
local controls_sx
local controls_dx
local jbutton
local sbutton
local life
local lifeCounter
local final_lifeCounter
local midground1
local midground2
local carica
local tree
local tap = 0
local tapS = 0
local slideTimer
local sign_go
local sign_slide
local sign_jump
local sign_trick
local tomba_portale
local timerCambiaScena
local change = 0
local eroeIsMorto = 0
local crate
local crate2
local blank_margin_crate
local blank_margin_crate2
local crate3
local blank_margin_crate3
local crate_direzione = 0
local crate3_direzione = 0
local limiteLivello
local limiteLivello_precedente
local partitaVinta = 0
local percorso_crate
local percorso2_crate
local percorso_crate3
local percorso2_crate3
local percorso_crate6
local percorso2_crate6
local blank_margin_crate_doppio
local crate4
local crate5
local blank_margin_crate6
local crate6
local crate6_direzione = 0
local blank_margin_crate7
local crate7
local crate7_visibile = 1
local timer_crate7
local blank_margin_crate8
local crate8
local blank_margin_crate9
local crate9
local blank_margin_crate_doppio2
local crate10
local crate11
local blank_margin_tomba
local checkpoint
local final_checkpoint
local progresso = 3
local final_progresso
local moved_attivo = 0



contaTouch = 0
collTerr = 0
verControl = 1
isSaltato = 0 -- serve per capire se ha saltato (per settare alcune proprieta)
inizioPartita = 0 -- serve per evitare accumuli di listener
isFermo = 1 -- serve a differenziare salto normale da salto con direzione
direzione = 0 -- serve per capire la direzione del salto (sx o dx)
premuto = 0
contaDelaySlide = 0
local currentObject


-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------



local function getSequenceData()
  local sequenceData = {
    {
      name = "idle",
      sheet = eroeSheet1,
      frames = {2,3,4,5},
      time = 440,
      loopCount = 0,
    },

    {
      name = "run",
      sheet = eroeSheet1,
      frames = {7,8,9,10,11,12,13,14},
      time = 440,
      loopCount = 0,
    },

    {
      name = "jump",
      sheet = eroeSheet1,
      frames = {6},
      time = 440,
      loopCount = 0,
    },
    {
      name = "dead",
      sheet = eroeSheet1,
      frames = {1},
      time = 440,
      loopCount = 0,
    },
    {
      name = "slide",
      sheet = eroeSheet2,
      frames = {1,2,3,4,5,6,7,8,9,10},
      time = 1000,
      loopCount = 1,
    },
    

  }
  return sequenceData
end

local function saveLife()
  composer.setVariable("final_lifeCounter", lifeCounter )
  composer.setVariable("final_progresso",progresso)
  composer.setVariable("final_checkpoint",checkpoint)
end

local function loadLife()
  lifeCounter = composer.getVariable("final_lifeCounter")
  progresso =  composer.getVariable("final_progresso")
  checkpoint =  composer.getVariable("final_checkpoint")
end

local function cambiaScena ()
  if change == 0 then
    
    if partitaVinta ~= 1 then
      lifeCounter = lifeCounter -1
    end
    saveLife()
    --se hai vinto
    if partitaVinta == 1 then
      progresso = progresso + 1--agg
      checkpoint = 0
      lifeCounter = 3 --perchè è la fine
      saveLife()--agg
      composer.gotoScene( "carica_livello" )
    
    --vedi altre situazioni...
  elseif lifeCounter == 0 then 
      lifeCounter = 3
      progresso = 1 --agg
      checkpoint = 0
      saveLife()
      composer.gotoScene( "scena_sconfitta_definitiva" )
    else
      progresso = 3--agg
  
      saveLife()--agg
      composer.gotoScene( "scena_sconfitta" ) --si possono aggiungere effetti
   
  -- composer.gotoScene( "carica_livello" )
    end
    change = 1
  end
end


local function setProprietaEroe()
  eroe.speed = 300
end

local function setVelocitaEroe()
  eroe:setLinearVelocity(eroe.velocity, 0)

end


local stage = display.getCurrentStage()

-- funzione che determina il comportamento del pulsante del movimento e richiama 
-- anche la funzione del pulsante salto se premuto (multitouch)


local function vaiSx()
      if eroeIsMorto == 0 then --se l'eroe NON è morto
          if isSaltato == 0 then
              eroe:play()
              eroe.velocity = -eroe.speed  
              eroe.xScale = -0.3
              direzione = -1
              setVelocitaEroe()
          end
      end
end

local function onButtonTouchLeft(event)

  local phase = event.phase
  local button = event.target

  if eroeIsMorto == 1 then
    print("sono morto")
    button.isButtonPressed = false
    stage:setFocus(nil)
    return true
  end
  
  
  if event.phase == "began" then
    
      currentObject = event.target
  
      isFermo = 0
      print("premuto sx")
      premuto = 1
      direzione = -1
      eroe:pause()
      eroe:setSequence("run")
      eroe:play()
     display.getCurrentStage():setFocus( event.target )
     event.target.isFocus = true
     
     Runtime:addEventListener( "enterFrame", vaiSx )
     
   

 --   elseif event.phase == "moved" and event.target.isFocus then
      elseif event.phase == "moved" then 
          print("premuto moved sx: "..premuto)
          isFermo = 0
          tapS = 0 --se stai correndo abilita slide
          contaDelaySlide = 0
          direzione = -1
          
           
    if eroe.sequence == "idle" then
         eroe:pause()
         eroe:setSequence("run")
         eroe:play()
    end
      
    if moved_attivo == 0 then
      print(">>>ATTIVO RUNTIME SX<<<")
        Runtime:removeEventListener( "enterFrame", vaiSx )
        Runtime:addEventListener( "enterFrame", vaiSx )
        moved_attivo = 1
    end
          
        
    display.getCurrentStage():setFocus( nil )
    event.target.isFocus = false
    
    
    elseif event.phase == "ended" or event.phase == "cancelled" then
      print("rilasciato movimento")
      isFermo = 1
      direzione = 0
      premuto = 0
      attivo = 0
      contaDelaySlide = 0--agg
      if isFermo == 0 then
        eroe:setLinearVelocity( 0,0 ) 
      end
      moved_attivo = 0
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
      Runtime:removeEventListener( "enterFrame", vaiSx )
      display.getCurrentStage():setFocus( nil )
      event.target.isFocus = false

    
  end

  return true
end

local function vaiDx()
     
      if eroeIsMorto == 0 then --se l'eroe NON è morto
           if isSaltato == 0 then
              eroe:play()
              eroe.velocity = eroe.speed  
              eroe.xScale = 0.3
              direzione = 1
              setVelocitaEroe()
          end
      end
end

local function onButtonTouchRight(event)

  local phase = event.phase
  local button = event.target

  if eroeIsMorto == 1 then
    print("sono morto")
    button.isButtonPressed = false
    stage:setFocus(nil)
    return true
  end
  
  --if not eroe.sequence == "jump" then

  if event.phase == "began" then
      currentObject = event.target
    
      print("premuto dx")
      premuto = 1
      direzione = 1
      eroe:pause()
      eroe:setSequence("run")
      eroe:play()
   
      display.getCurrentStage():setFocus( event.target )
      event.target.isFocus = true
      
      Runtime:addEventListener( "enterFrame", vaiDx )
      
      
 --   elseif phase == "moved" and event.target.isFocus then
   elseif phase == "moved" then
    
      print("premuto moved dx: "..premuto)
      tapS = 0 --se stai correndo abilita slide
      contaDelaySlide = 0
      direzione = 1
      isFermo = 0
      
    if eroe.sequence == "idle" then
         eroe:pause()
         eroe:setSequence("run")
         eroe:play()
    end
     if moved_attivo == 0 then
        print(">>>ATTIVO RUNTIME DX<<<")
        Runtime:removeEventListener( "enterFrame", vaiDx )
        Runtime:addEventListener( "enterFrame", vaiDx )
        moved_attivo = 1
     end
   
     display.getCurrentStage():setFocus( nil )
     event.target.isFocus = false
      
    
    elseif event.phase == "ended" or phase == "cancelled" then
      
      print("rilasciato movimento")
      isFermo = 1
      direzione = 0
      premuto = 0
      attivo = 0
      contaDelaySlide = 0 --agg
      moved_attivo = 0
      if isFermo == 0 then
        eroe:setLinearVelocity( 0,0 ) 
      end
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
      
      Runtime:removeEventListener( "enterFrame", vaiDx )
      display.getCurrentStage():setFocus( nil )
      event.target.isFocus = false
  end

  return true
end



local function onLocalCollisionPlatform( self, event )

if event.other == eroe and event.other.isVisible == true then
  
  if ( event.phase == "began" ) then

      print("collisione platform inizio")
       eroe:pause()
       eroe:setSequence("idle")
       eroe:play()
     
      tap = 0
      print("iniziopartita: "..inizioPartita)
   
      if isSaltato == 1 and isFermo == 0 then
        eroe:setLinearVelocity(0,0) --togli accelerazione causata da salto + slide
        isSaltato = 0
        isFermo = 1
        contaDelaySlide = 0 
        --direzione = 0
        premuto = 0
        moved_attivo = 0
        if direzione == 1 then
        print("CON CHI CREDI DI AVERE A CHE FARE? -->DX")
        isSaltato = 0
        controls_dx:dispatchEvent( { name = "touch", target = controls_dx, phase = "moved" } )
         
        else
        isSaltato = 0
        print("CON CHI CREDI DI AVERE A CHE FARE?-->SX")
        controls_sx:dispatchEvent( { name = "touch", target = controls_sx, phase = "moved" } )
          
        end
      

      elseif isSaltato == 1 and isFermo == 1 then
    
        isSaltato = 0
        contaDelaySlide = 0 
      end

    
  elseif ( event.phase == "ended"  ) then

    print("collisione platform fine") 
     isSaltato = 1
    
    
    if collTerr == 0 and isSaltato == 0 then
      eroe:pause()
      eroe:setSequence("idle")
      eroe:play()
    end
    tapS = 0
    contaDelaySlide = 0

  end
end

end


local function onLocalCollisionPercorso_crate (self, event)
  
    if ( event.phase == "began" ) then

        if event.other == crate3 and event.other.isVisible == true then
            print("COLLISIONE CRATE3")
            crate3_direzione = -  crate3_direzione
            if  crate3_direzione == 0 then
              print("MUOVITI A DX") --toccato muro sx
              crate3:setLinearVelocity(80,0)
              blank_margin_crate3:setLinearVelocity(80,0)
              crate3_direzione = 1
            else
               print("MUOVITI A SX")--toccato muro dx
              crate3:setLinearVelocity(-80,0)
              blank_margin_crate3:setLinearVelocity(-80,0)
              crate3_direzione = 0
            
          end
          
            elseif event.other == crate and event.other.isVisible == true then
            print("COLLISIONE CRATE")
            crate_direzione = - crate_direzione
            if crate_direzione == 0 then
              print("MUOVITI SU") --toccato muro sx
              crate:setLinearVelocity(0,-100)
              blank_margin_crate:setLinearVelocity(0,-100)
              crate_direzione = 1
            else
               print("MUOVITI GIU")--toccato muro dx
              crate:setLinearVelocity(0,100)
              blank_margin_crate:setLinearVelocity(0,100)
              crate_direzione = 0
            end
            
            
            elseif event.other == crate6 and event.other.isVisible == true then
            print("COLLISIONE CRATE6")
            crate6_direzione = - crate6_direzione
            if crate6_direzione == 0 then
              print("MUOVITI ANG_DX") --toccato muro sx
              crate6:setLinearVelocity(50,50)
              blank_margin_crate6:setLinearVelocity(50,50)
              crate6_direzione = 1
            else
               print("MUOVITI ANG_SX")--toccato muro dx
              crate6:setLinearVelocity(-50,-50)
              blank_margin_crate6:setLinearVelocity(-50,-50)
              crate6_direzione = 0
            end
          
        end
  
    end
end


function jumpButton( event )
  if ( event.phase == "began" ) then
    print("INZIO FASE SALTO")
   
    display.getCurrentStage():setFocus( event.target )
    event.target.isFocus = true
    eroe:pause()
    eroe:setSequence("jump")
    eroe:play() 
  
    Runtime:removeEventListener( "enterFrame", vaiSx )
    Runtime:removeEventListener( "enterFrame", vaiDx )

  isSaltato = 1
  inizioPartita = 1
  print("-->isFermo: "..isFermo)

  if tap == 0 then
    print("direzione: "..direzione)

    if isFermo == 0 then
      if direzione < 0 then
        eroe.xScale = -0.3
        eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y)
      else
        eroe.xScale = 0.3
        eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y)
      end
    end
    if isFermo == 1 then
      eroe:applyLinearImpulse(0,-300,eroe.x,eroe.y) 
    end
    tap = 1
  end
     
    elseif ( event.phase == "moved" ) then
 
        elseif event.phase == "ended" then
            print("FINE FASE SALTO")
            
            Runtime:removeEventListener( "enterFrame", vaiSx )
            Runtime:removeEventListener( "enterFrame", vaiDx )
            
            display.getCurrentStage():setFocus( nil )
            event.target.isFocus = false
        end

 
    return true
end

--sprite listener per eroe
local function spriteListener(event)

  if eroe.sequence == "slide" and eroe.frame == 10 then
    
    eroe:setSequence("idle")
    eroe:play()

    if slideTimer then
      print("cancello timer")
      timer.cancel(slideTimer)
      slideTimer = nil
    end
    slideTimer = timer.performWithDelay(200, attivaSlide,  1)
  end

  if eroe.sequence == "dead" then
    physics.removeBody(eroe)
    local nw, nh = eroe.width*0.3*0.5, eroe.height*0.3*0.5 --per custom shape
    physics.addBody(eroe, "dynamic", {density = 1.5, friction = 0.3, bounce=0, shape={-nw,-nh,nw,-nh,nw,nh-70,-nw,nh-70}})
    timerCambiaScena = timer.performWithDelay(2000, cambiaScena,  1) --era 2000
  end
  
    --hack per risolvere bug controlli su android 
    if eroe.sequence == "idle" or eroe.sequence == "jump" or eroe.sequence == "slide" then
        Runtime:removeEventListener( "enterFrame", vaiSx )
        Runtime:removeEventListener( "enterFrame", vaiDx )
    end
    
    if eroe.sequence == "run" then
        contaDelaySlide = 0
    end
    
     if eroe.sequence == "idle" then
        moved_attivo = 0
    end

end

function attivaSlide()
  contaDelaySlide = 0
  print("attiva slide")
end


function slideButton( event )
  print("-->isFermoS: "..isFermo)
  print("-->tapS: "..tapS)
  print("-->isSaltatoS: "..isSaltato)
  print("-->contaDelaySlideS: "..contaDelaySlide)
  
  if ( event.phase == "began" ) then
    if isFermo == 0  and tapS == 0 and isSaltato == 0 and contaDelaySlide == 0 then --se non è fermo e non è in salto allora
    print("INZIO FASE SLIDE")
    display.getCurrentStage():setFocus( event.target )
    event.target.isFocus = true
    eroe:pause()
    eroe:setSequence("slide")
    eroe:play()
 
   Runtime:removeEventListener( "enterFrame", vaiDx )
   Runtime:removeEventListener( "enterFrame", vaisx )
    
    verContol = 0
  
    if direzione < 0 then
      eroe.xScale = -0.3
      eroe:applyLinearImpulse(-200,0,eroe.x,eroe.y)
    else
      eroe.xScale = 0.3
      eroe:applyLinearImpulse(200,0,eroe.x,eroe.y)
    end

    tapS = 1
    contaDelaySlide = 1
  
  end
      elseif ( event.phase == "moved" ) then
  
         elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
            print("FINE FASE SLIDE")
            Runtime:removeEventListener( "enterFrame", vaiDx )
            Runtime:removeEventListener( "enterFrame", vaisx )
            display.getCurrentStage():setFocus( nil )
            event.target.isFocus = false
        end
  
    return true
end



function fineLivelloCollision(self, event)
  if ( event.phase == "began" ) then


    if event.other == eroe and eroe.sequence ~= "dead"  then
      print("VAI A LIVELLO 2")
      partitaVinta = 1
      rimuoviJbutton()
      rimuoviSbutton()
      rimuoviCollisioneBurrone()
      rimuoviCollisionePlatform()
      cambiaScena ()
  end
  
  end

end

local function onLocalCollisionBurrone( self, event )

  if ( event.phase == "began" ) then


    if event.other == eroe and event.other.isVisible == true then
      
      rimuoviJbutton()
      rimuoviSbutton()
      rimuoviCollisioneBurrone()
      rimuoviCollisionePlatform()
      cambiaScena ()
      
    end
    
    
  end
  
end

local function nascondiCrate()
  if crate7_visibile == 1 then
    crate7.isVisible = false
    blank_margin_crate7.isVisible = false

  else
    crate7.isVisible = true
    blank_margin_crate7.isVisible = true
  end
  
end

local function onLocalCollisionTomba (self, event)
  
    if ( event.phase == "began" ) then
        if event.other == eroe and checkpoint == 0 then
            checkpoint = 1
            composer.setVariable("final_checkpoint",checkpoint)
            display.remove(tomba_portale)
            tomba_portale = nil
          
            print("SETTATO CHECKPOINT!")
            
        end
      
    end
    
end



--funzioni per rimuovere e aggiungere l'event listener del pulsante del movimento per evitare di ricevere event quando l'eroe si trova nella fase di salto

--funzioni per rimuovere e aggiungere l'event listener del pulsante del movimento per evitare di ricevere event quando l'eroe si trova nella fase di salto

function rimuoviControls()
  Runtime:removeEventListener( "enterFrame", vaiSx )
  Runtime:removeEventListener( "enterFrame", vaiDx )
  controls_dx:removeEventListener("touch", onButtonTouchRight )
  controls_sx:removeEventListener("touch", onButtonTouchLeft )
end

function rimuoviJbutton()
  jbutton:removeEventListener("touch", jumpButton )
end

function rimuoviSbutton()
  sbutton:removeEventListener("touch", slideButton )
end

function rimuoviCollisioneTerreno()
  terreno:removeEventListener( "collision" )
end
function rimuoviCollisioneBurrone()
  burrone:removeEventListener( "collision" )
end

function rimuoviCollisionePlatform()
  blank_margin_crate:removeEventListener( "collision" )
  blank_margin_crate2:removeEventListener( "collision" )
  blank_margin_crate3:removeEventListener( "collision" )
  blank_margin_crate6:removeEventListener( "collision" )
  blank_margin_crate7:removeEventListener( "collision" )
  blank_margin_crate8:removeEventListener( "collision" )
  blank_margin_crate9:removeEventListener( "collision" )
  blank_margin_crate_doppio:removeEventListener( "collision" )
  blank_margin_crate_doppio2:removeEventListener( "collision" )
  blank_margin_tomba:removeEventListener( "collision" )
 
end

function attivaControls()
  controls_dx:addEventListener("touch", onButtonTouchRight )
  controls_sx:addEventListener("touch", onButtonTouchLeft )
end




--verifico se e' la seconda partita
carica = composer.getVariable("final_lifeCounter")
if(carica ~= nil) then
  loadLife()

else
  lifeCounter = 3
  final_lifeCounter = 0
  checkpoint = 0
  saveLife() --final_lifeCounter diventa 3
end

-- create()
function scene:create( event )


  physics.pause()  -- Temporarily pause the physics engine

  --display.setDefault("background", 0,0,255) --per cio che viene oltre lo sfondo

  local sceneGroup = self.view
  -- Code here runs when the scene is first created but has not yet appeared on screen
  -- Set up display groups
 -- local backGroup = display.newGroup()  -- Display group per sfondo
 -- local mainGroup = display.newGroup()  -- Display group per personaggio spari nemici etc oggetti
 -- local uiGroup = display.newGroup()    -- Display group for UI objects like the score
 
  --sceneGroup:insert( backGroup )  -- Insert into the scene's view group
  --sceneGroup:insert( mainGroup )  -- Insert into the scene's view group
 -- sceneGroup:insert( uiGroup )    -- Insert into the scene's view group

  display.setStatusBar(display.HiddenStatusBar)
  
  background1 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) --prima 800x600
  background1.x = display.contentCenterX-15
  background1.y = display.contentCenterY-200


  background2 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) 
  background2.x = display.contentCenterX-15 + background1.width
  background2.y = display.contentCenterY-200
  
  background3 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) --prima 800x600
  background3.x = display.contentCenterX-15 + background1.width + background2.width
  background3.y = display.contentCenterY-200


  background4 = display.newImageRect( sceneGroup, "BG_scroll_NEW.png", 1000, 1140) 
  background4.x = display.contentCenterX-15 + background1.width + background2.width + background3.width
  background4.y = display.contentCenterY-200

  burrone = display.newImageRect( sceneGroup, "vuoto.png", 7000, 10 ) --5000
  burrone.x = display.contentCenterX
  burrone.y = display.contentHeight-100-- -100

  --[[ 
 -- creo l'erore (per ora un rect) -->INSERIRE PER ANIMAZIONE E CAMBIO ANIMAZIONE
 -- eroe = display.newRect(0,0,60,50)
  eroe =   display.newImageRect("personaggio2.png",79,120) --invece di questo devi creare un animazione (idle), e poi diverse che vai a cambiare come toccapallone vedi ver dekstop, nota: un animazione puo avere anche solo una png!
  eroe.x = display.contentCenterX
  eroe.y = display.contentHeight-470
  eroe.isVisible = true
  --eroe.xScale = -1
  eroe.id = "eroe"
  --]]

--animazione eroe

  local eroeSequenze = getSequenceData()
  eroe = display.newSprite(sceneGroup, eroeSheet1, eroeSequenze)
  --eroe.x = display.contentCenterX-250 
  --eroe.y = display.contentHeight-600
  local scaleX = 0.3
  local scaleY = 0.3
  eroe:scale(scaleX,scaleY)
  local nw, nh = eroe.width*scaleX*0.5, eroe.height*scaleY*0.5 --per custom shape


  eroe.id = "eroe"
  eroe:play()
--eroe:setSequence("dead")

  --proprieta eroe
  setProprietaEroe()

  --creo i controlli per spostamento

  controls_sx = display.newImageRect("freccia_sx.png",73,70) --erano 53,50
  controls_sx.x = display.contentCenterX-340
  controls_sx.y = display.contentHeight-335
  controls_sx.id = "sx"
  
  controls_dx = display.newImageRect("freccia_dx.png",73,70)
  controls_dx.x = display.contentCenterX-200
  controls_dx.y = display.contentHeight-335
  controls_dx.id = "dx"

  --creo pulsanti per salto e slide (non li inserisco in sceneGroup)

  jbutton = display.newImageRect("jbutton_img.png",130,80)
  jbutton.x = display.contentCenterX+345
  jbutton.y = display.contentHeight-338
  jbutton.id = "jump"

  sbutton = display.newImageRect("sbutton_img.png",130,80)
  sbutton.x = display.contentCenterX+235
  sbutton.y = display.contentHeight-338
  sbutton.id = "slide"

  --creo interfaccia (non la inserisco in sceneGroup)
  life = display.newImageRect("ui_life.png",60,61)
  life.x = display.contentCenterX+260
  life.y = display.contentHeight-695
  life.id = "life"

  lifeText = display.newEmbossedText("x "..lifeCounter, 720, 345, "plasdrip.ttf", 40)
  lifeText:setFillColor( 243/255, 182/255, 58/255 )

  -- aggiungo oggetti a sfondo
  
  blank_margin_crate = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate.x = display.contentCenterX+400
  blank_margin_crate.y = display.contentHeight-635
  crate = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate.x = display.contentCenterX+400
  crate.y = display.contentHeight-580
 
  --crate iniziale 
  blank_margin_crate2 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate2.x = display.contentCenterX-325
  blank_margin_crate2.y = display.contentHeight-436
  crate2 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate2.x = display.contentCenterX-325
  crate2.y = display.contentHeight-380
  
  blank_margin_crate3 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate3.x = display.contentCenterX+300
  blank_margin_crate3.y = display.contentHeight-436
  crate3 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate3.x = display.contentCenterX+300
  crate3.y = display.contentHeight-380
  
  blank_margin_crate_doppio = display.newImageRect(sceneGroup, "vuoto.png", 206, 10)
  blank_margin_crate_doppio.x = display.contentCenterX+650
  blank_margin_crate_doppio.y = display.contentHeight-900
  crate4 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate4.x = display.contentCenterX+600
  crate4.y = display.contentHeight-850
  crate5 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate5.x = display.contentCenterX+700
  crate5.y = display.contentHeight-850
  
  blank_margin_crate6 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate6.x = display.contentCenterX+950
  blank_margin_crate6.y = display.contentHeight-900
  crate6 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate6.x = display.contentCenterX+950
  crate6.y = display.contentHeight-850
  
  blank_margin_crate7 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate7.x = display.contentCenterX+1400
  blank_margin_crate7.y = display.contentHeight-700
  crate7 = display.newImageRect(sceneGroup, "crate_delicato.png", 106, 106) 
  crate7.x = display.contentCenterX+1400
  crate7.y = display.contentHeight-650
  
  blank_margin_crate8 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate8.x = display.contentCenterX+1650
  blank_margin_crate8.y = display.contentHeight-700
  crate8 = display.newImageRect(sceneGroup, "crate_delicato.png", 106, 106) 
  crate8.x = display.contentCenterX+1650
  crate8.y = display.contentHeight-650
  
  blank_margin_crate9 = display.newImageRect(sceneGroup, "vuoto.png", 105, 10)
  blank_margin_crate9.x = display.contentCenterX+1950
  blank_margin_crate9.y = display.contentHeight-700
  crate9 = display.newImageRect(sceneGroup, "crate_delicato.png", 106, 106) 
  crate9.x = display.contentCenterX+1950
  crate9.y = display.contentHeight-650
  
  blank_margin_crate_doppio2 = display.newImageRect(sceneGroup, "vuoto.png", 206, 10)
  blank_margin_crate_doppio2.x = display.contentCenterX+2200
  blank_margin_crate_doppio2.y = display.contentHeight-700
  crate10 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate10.x = display.contentCenterX+2150
  crate10.y = display.contentHeight-650
  crate11 = display.newImageRect(sceneGroup, "crate.png", 106, 106) 
  crate11.x = display.contentCenterX+2250
  crate11.y = display.contentHeight-650

  
  
   --setto percorso crate
  percorso_crate = display.newImageRect(sceneGroup, "vuoto.png", 100, 10)
  percorso_crate.x = display.contentCenterX+400
  percorso_crate.y = display.contentHeight-920
  percorso2_crate = display.newImageRect(sceneGroup, "vuoto.png", 100, 10)
  percorso2_crate.x = display.contentCenterX+400
  percorso2_crate.y = display.contentHeight-100

  
  --setto percorso crate3
  percorso_crate3 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso_crate3.x = display.contentCenterX-200
  percorso_crate3.y = display.contentHeight-380
 
  percorso2_crate3 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso2_crate3.x = display.contentCenterX+720 --+500
  percorso2_crate3.y = display.contentHeight-380
  
  --setto percorso crate6
  percorso_crate6 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso_crate6.x = display.contentCenterX+950 -- +950  CAMBIATO ORA
  percorso_crate6.y = display.contentHeight-950
 
  percorso2_crate6 = display.newImageRect(sceneGroup, "vuoto.png", 10, 100)
  percorso2_crate6.x = display.contentCenterX+1200 --+500
  percorso2_crate6.y = display.contentHeight-650
  
  blank_margin_tomba = display.newImageRect(sceneGroup, "vuoto.png", 53, 76) 
  blank_margin_tomba.x = display.contentCenterX+705
  blank_margin_tomba.y = display.contentHeight-940
 
  tomba_portale = display.newImageRect(sceneGroup, "tomb_bonus.png", 53, 76) 
  tomba_portale.x = display.contentCenterX+705
  tomba_portale.y = display.contentHeight-940
  
  sign_trick = display.newImageRect(sceneGroup, "trick_sign.png", 91, 93) 
  sign_trick.x = display.contentCenterX+580
  sign_trick.y = display.contentHeight-945
  
  sign_go = display.newImageRect(sceneGroup, "sign_go.png", 91, 93) 
  sign_go.x = display.contentCenterX+2250
  sign_go.y = display.contentHeight-750
  
 
 
  --aggiungo limite livello
  limiteLivello= display.newImageRect(sceneGroup, "vuoto.png", 10, 2000)
  limiteLivello.x = display.contentCenterX+2300
  limiteLivello.y = display.contentHeight-450
  
  limiteLivello_precedente= display.newImageRect(sceneGroup, "vuoto.png", 10, 1000)
  limiteLivello_precedente.x = display.contentCenterX-375
  limiteLivello_precedente.y = display.contentHeight-450
  
  
  --aggiungo gli oggetti alla sceneGroup nel giusto ordine, back to front

  sceneGroup:insert( background1 )
	sceneGroup:insert( background2 )
  sceneGroup:insert( burrone )
 -- sceneGroup:insert( spike )
  sceneGroup:insert( blank_margin_crate2 )
  sceneGroup:insert( crate2 )
  sceneGroup:insert( blank_margin_crate3 )
  sceneGroup:insert( crate3 )
  sceneGroup:insert( blank_margin_crate )
  sceneGroup:insert( crate )
  sceneGroup:insert( blank_margin_crate_doppio )
  sceneGroup:insert( crate4 )
  sceneGroup:insert( crate5 )
  sceneGroup:insert( blank_margin_crate6 )
  sceneGroup:insert( crate6 )
  sceneGroup:insert( blank_margin_crate7 )
  sceneGroup:insert( crate7 )
  sceneGroup:insert( blank_margin_crate8 )
  sceneGroup:insert( crate8 )
  sceneGroup:insert( blank_margin_crate9 )
  sceneGroup:insert( crate9 )
  sceneGroup:insert( blank_margin_crate_doppio2 )
  sceneGroup:insert( crate10 )
  sceneGroup:insert( crate11 )
  sceneGroup:insert( tomba_portale )
  sceneGroup:insert( blank_margin_tomba )
  sceneGroup:insert( sign_trick )
  sceneGroup:insert( sign_go )
  sceneGroup:insert( limiteLivello )
  sceneGroup:insert( limiteLivello_precedente )
  sceneGroup:insert( percorso_crate )
  sceneGroup:insert( percorso2_crate )
  sceneGroup:insert( percorso_crate3 )
  sceneGroup:insert( percorso2_crate3 )
  sceneGroup:insert( percorso_crate6 )
  sceneGroup:insert( percorso2_crate6 )
	sceneGroup:insert( eroe )

 -- sceneGroup:insert( life )
--  sceneGroup:insert( lifeText )
--  sceneGroup:insert( controls)
--	sceneGroup:insert( jbutton )
--  sceneGroup:insert( sbutton )
  -- non li inserisco, quindi li rimuovo manulamente in scene:hide

  --aggiungo camera, più piccolo il layer più è vicino
  -- se aggiungi ricorda di cambiare il limite da 12 layer
    camera:add(eroe,1,true) -- layer 1 in focus
    camera:add(burrone,2,false) -- layer 2 non in focus
    camera:add(percorso_crate,3,false)
    camera:add(percorso2_crate,4,false)
    camera:add(percorso_crate3,5,false)
    camera:add(percorso2_crate3,6,false)
    camera:add(percorso_crate6,7,false)
    camera:add(percorso2_crate6,8,false)
    camera:add(limiteLivello_precedente,9,false)
    camera:add(blank_margin_crate2,10,false)
    camera:add(crate2,11,false)
    camera:add(blank_margin_crate3,12,false)
    camera:add(crate3,13,false)
    camera:add(blank_margin_crate,14,false)
    camera:add(crate,15,false)
    camera:add(blank_margin_crate_doppio,16,false)
    camera:add(crate4,17,false)
    camera:add(crate5,18,false)
    camera:add(blank_margin_crate6,19,false)
    camera:add(crate6,20,false)
    camera:add(blank_margin_crate7,21,false)
    camera:add(crate7,22,false)
    camera:add(blank_margin_crate8,23,false)
    camera:add(crate8,24,false)
    camera:add(blank_margin_crate9,25,false)
    camera:add(crate9,26,false)
    camera:add(blank_margin_crate_doppio2,27,false)
    camera:add(crate10,28,false)
    camera:add(crate11,29,false)
    camera:add(tomba_portale,30,false)
    camera:add(blank_margin_tomba,31,false)
    camera:add(sign_trick,32,false)
    camera:add(sign_go,33,false)
    camera:add(limiteLivello,34,false)
    camera:add(background1,35,false) -- layer 3 non in focus
    camera:add(background2,36,false) -- layer 3 non in focus
    camera:add(background3,37,false) -- layer 3 non in focus
    camera:add(background4,38,false) -- layer 3 non in focus
    
    
 -- camera:layer(12).parallaxRatio = 0.5--terreno si muove piu veloce dello sfondo effetto 3d

  camera:setBounds(display.contentWidth/2,2300, 0 , display.contentHeight-500)

  -- aggiungo fisica
  physics.addBody(burrone, "static", { bounce=0,friction = 1})
  burrone.isSensor = true
  physics.addBody(eroe, "dynamic", {density = 1.5, friction = 0.7, bounce=0, shape={-nw+50,-nh+20,nw-10,-nh+20,nw-30,nh,-nw+60,nh}})
 
  
  
  -- se muore zombieM.anchorY = 0.45
 -- physics.addBody(spike,"static", { shape={60,60,-60,0,0,-30,60,0,-60,60} } )
  physics.addBody(crate2,"static", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate2,"static", { bounce=0,friction = 3 })
  physics.addBody(crate3,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate3,"kinematic", { bounce=0,friction = 3 })
  physics.addBody(crate,"kinematic", { bounce=0,friction = 1 } )
  physics.addBody(blank_margin_crate,"kinematic", { bounce=0,friction = 1 })
  
  physics.addBody(blank_margin_crate_doppio,"kinematic", { bounce=0,friction = 3 })
  physics.addBody(crate4,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(crate5,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate6,"kinematic", { bounce=0,friction = 3 })
  physics.addBody(crate6,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate7,"dynamic", { density = 20, bounce=0,friction = 3 })
  physics.addBody(crate7,"dynamic", { density = 20,bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate8,"dynamic", {density = 20, bounce=0,friction = 3 })
  physics.addBody(crate8,"dynamic", {density = 20, bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate9,"dynamic", {density = 20, bounce=0,friction = 3 })
  physics.addBody(crate9,"dynamic", {density = 20, bounce=0,friction = 3 } )
  physics.addBody(blank_margin_crate_doppio2,"kinematic", { bounce=0,friction = 3 })
  physics.addBody(crate10,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(crate11,"kinematic", { bounce=0,friction = 3 } )
  physics.addBody(percorso_crate,"dynamic", { bounce=0})
  physics.addBody(percorso2_crate,"dynamic", { bounce=0})
  physics.addBody(percorso_crate3,"dynamic")
  physics.addBody(percorso2_crate3,"dynamic")
  physics.addBody(percorso_crate6,"dynamic",{ bounce=0})
  physics.addBody(percorso2_crate6,"dynamic",{ bounce=0})
  physics.addBody(blank_margin_tomba,"static", { bounce=0,friction = 0 })
  blank_margin_tomba.isSensor = true
  
  blank_margin_crate2.isSensor = true 
  blank_margin_crate3.isSensor = true 
  blank_margin_crate.isSensor = true 
  blank_margin_crate_doppio.isSensor = true 
  blank_margin_crate6.isSensor = true 
  blank_margin_crate7.isSensor = true 
  blank_margin_crate_doppio2.isSensor = true 
  blank_margin_crate8.isSensor = true 
  blank_margin_crate9.isSensor = true 

  
  physics.addBody(limiteLivello,"static")
  limiteLivello.isSensor = true
  physics.addBody(limiteLivello_precedente,"static")

  eroe.isFixedRotation = true
  eroe.gravityScale = 8
  crate7.gravityScale = 0
  blank_margin_crate7.gravityScale = 0
  crate8.gravityScale = 0
  blank_margin_crate8.gravityScale = 0
  crate9.gravityScale = 0
  blank_margin_crate9.gravityScale = 0
  crate7.isFixedRotation = true
  crate8.isFixedRotation = true
  crate9.isFixedRotation = true


  
  percorso_crate.gravityScale = 0
  percorso2_crate.gravityScale = 0
  percorso_crate3.gravityScale = 0
  percorso2_crate3.gravityScale = 0
  percorso_crate6.gravityScale = 0
  percorso2_crate6.gravityScale = 0
  percorso_crate.isSensor = true
  percorso2_crate.isSensor = true
  percorso_crate3.isSensor = true
  percorso2_crate3.isSensor = true
  percorso_crate3.isSensor = true
  percorso2_crate3.isSensor = true
  percorso_crate6.isSensor = true
  percorso2_crate6.isSensor = true
  
  percorso_crate.isFixedRotation = true
  percorso2_crate.isFixedRotation = true
  percorso_crate3.isFixedRotation = true
  percorso2_crate3.isFixedRotation = true
  percorso_crate6.isFixedRotation = true
  percorso2_crate6.isFixedRotation = true
  
  
  --mettiamo in movimento i crate

  blank_margin_crate3:setLinearVelocity(-80,0)
  crate3:setLinearVelocity(-80,0)

  blank_margin_crate:setLinearVelocity(0,100)
  crate:setLinearVelocity(0,100)
  


 --PER DEBUG MEMORY LEAK
 -- timerCambiaScena = timer.performWithDelay(2000, cambiaScena,  1)
 
 

end


-- show()
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase


  if ( phase == "will" ) then
    -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    if checkpoint == 0 then
      
      eroe.x = display.contentCenterX-320
      eroe.y = display.contentHeight-520
    else

      eroe.x = display.contentCenterX+600 -- -320
      eroe.y = display.contentHeight-1000 -- -620
      display.remove(tomba_portale)
      tomba_portale = nil
    end

    


  
elseif ( phase == "did" ) then
    print( collectgarbage( "count" ) / 1024 ) -- default is to show you Kilobytes of memory, this converts it to Megabytes of memory
    print( system.getInfo( "textureMemoryUsed" ) / (1024 * 1024) ) -- defaults to bytes, this converts it to Megabytes.
    print("**********BENVENUTO A LEVEL3**********")
    physics.start()
    camera:track()
  --  physics.setDrawMode( "hybrid" )
    controls_sx:addEventListener("touch", onButtonTouchLeft )
    controls_dx:addEventListener("touch", onButtonTouchRight )
    jbutton:addEventListener("touch", jumpButton )
    sbutton:addEventListener("touch", slideButton )
    eroe:addEventListener("sprite",spriteListener)
    
    burrone.collision = onLocalCollisionBurrone
    burrone:addEventListener( "collision" )
    
    blank_margin_tomba.collision = onLocalCollisionTomba
    blank_margin_tomba:addEventListener( "collision" )
    blank_margin_crate2.collision = onLocalCollisionPlatform
    blank_margin_crate2:addEventListener( "collision" )
    blank_margin_crate3.collision = onLocalCollisionPlatform
    blank_margin_crate3:addEventListener( "collision" )
    blank_margin_crate.collision = onLocalCollisionPlatform
    blank_margin_crate:addEventListener( "collision" )
    blank_margin_crate6.collision = onLocalCollisionPlatform
    blank_margin_crate6:addEventListener( "collision" )
    blank_margin_crate7.collision = onLocalCollisionPlatform
    blank_margin_crate7:addEventListener( "collision" )
    blank_margin_crate8.collision = onLocalCollisionPlatform
    blank_margin_crate8:addEventListener( "collision" )
    blank_margin_crate9.collision = onLocalCollisionPlatform
    blank_margin_crate9:addEventListener( "collision" )
    
    blank_margin_crate_doppio.collision = onLocalCollisionPlatform
    blank_margin_crate_doppio:addEventListener( "collision" )
    blank_margin_crate_doppio2.collision = onLocalCollisionPlatform
    blank_margin_crate_doppio2:addEventListener( "collision" )
    
    
    percorso_crate.collision = onLocalCollisionPercorso_crate
    percorso_crate:addEventListener( "collision" )
    percorso2_crate.collision = onLocalCollisionPercorso_crate
    percorso2_crate:addEventListener( "collision" )
    
    percorso_crate3.collision = onLocalCollisionPercorso_crate
    percorso_crate3:addEventListener( "collision" )
    percorso2_crate3.collision = onLocalCollisionPercorso_crate
    percorso2_crate3:addEventListener( "collision" )
    
    percorso_crate6.collision = onLocalCollisionPercorso_crate
    percorso_crate6:addEventListener( "collision" )
    percorso2_crate6.collision = onLocalCollisionPercorso_crate
    percorso2_crate6:addEventListener( "collision" )

    limiteLivello.collision = fineLivelloCollision
    limiteLivello:addEventListener("collision")
    
  
    
    
    
    --CONTROLLO LIMITE PULSANTI DI CONTROLS
    Runtime:addEventListener( "touch", function(e)
      if currentObject ~= nil then
        if currentObject.contentBounds ~= nil then
             if e.x < currentObject.contentBounds.xMin or
                e.x > currentObject.contentBounds.xMax or
                e.y < currentObject.contentBounds.yMin or
                e.y > currentObject.contentBounds.yMax then
                print("Its out")
                Runtime:removeEventListener( "enterFrame", vaiDx )
                Runtime:removeEventListener( "enterFrame", vaiSx )
      
           end
        end
      end
end)
    

    -- Code here runs when the scene is entirely on screen

  end
end


-- hide()
function scene:hide( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then
    -- Code here runs when the scene is on screen (but is about to go off screen)

    if timerCambiaScena ~=  nil then
      timer.cancel(timerCambiaScena)
      timerCambiaScena = nil
    end
    if timerSlide ~= nil then
      timer.cancel(timerSlide)
      timerSlide = nil
    end


  elseif ( phase == "did" ) then
    -- Code here runs immediately after the scene goes entirely off screen
    -- physics.pause()
    physics.stop()
    rimuoviControls()
    eroe:removeEventListener("sprite",spriteListener)
    percorso_crate:removeEventListener( "collision" )
    percorso2_crate:removeEventListener( "collision" )
    percorso_crate3:removeEventListener( "collision" )
    percorso2_crate3:removeEventListener( "collision" )

    limiteLivello:removeEventListener("collision")
    Runtime:removeEventListener( "touch", function(e)end)
    
    --rimuovo manualmente GUI
    display.remove(life)
    life = nil
    display.remove(lifeText)
    lifeText = nil
    display.remove(controls_sx)
    controls_sx = nil
    display.remove(controls_dx)
    controls_dx = nil
    display.remove(jbutton)
    jbutton = nil
    display.remove(sbutton)
    sbutton = nil
    --rimuovo camera
    camera:destroy()
    camera=nil
    --rimuovo scena
    composer.removeScene( "level3" )
    

  end
end


-- destroy()
function scene:destroy( event )

  local sceneGroup = self.view
  -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
