local composer = require( "composer" )
local scene = composer.newScene()

display.setStatusBar(display.HiddenStatusBar)
audio.reserveChannels(1)
--audio.stop(1)


-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
--qui sotto mettiamo tutte le dichiarazioni alle variabili
local background
local playbutton
local playcredit
local backsound
local stopmusic
local newGameButton
local newGameButton2
local newGameButton3
local appmusic=0
local temporaneo
local nuovo
local fb
local video=native.newVideo(display.contentCenterX,display.contentCenterY,850,480)
local sp
local im
local logo
local torcia
local flashlight = require('plugin.flashlight')
local vibrator = require('plugin.vibrator')
local videoo=native.newVideo(display.contentCenterX,display.contentCenterY,850,480)
local ultimo


--system.openURL( "http://www.coronalabs.com" )
--Per aggiungere che apre un browser lasciando app

--local webView = native.newWebView( display.contentCenterX, display.contentCenterY, 320, 480 )
--webView:request( "http://www.coronalabs.com/" )
--Per aprire all'interno dell'app

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
--funzioni

--local function cambiaScena ()
--composer.gotoScene( "story2" ) --si possono aggiungere effetti
--end


local function cambiaScena ()
composer.gotoScene( "menucopia" ) --si possono aggiungere effetti
end

local function cambiaScena3 ()
composer.gotoScene("catch")
end

local function cambiaScena2 ()
composer.gotoScene( "giocos" ) --si possono aggiungere effetti
end

local function accendiTorcia()
flashlight.on()
end

local function cambiaScena4 ()

composer.gotoScene("level1")
end




local function onScreensTouch (event)
videoo:pause()
videoo:removeSelf()
videoo=nil
imm.isHitTestable=false
composer.gotoScene("menu")

end



local function videocredits()
video:load("crediti.mp4")
video:play()
--sceneGroup:insert(videoo)
--imm=display.newRect(display.contentCenterX,display.contentCenterY,800,800)
--imm.alpha=0
--imm.isHitTestable=true
--imm:addEventListener("tap",onScreensTouch)
composer.gotoScene("menu")
end

local function onScreenTouch (event)
video:pause()
video:removeSelf()
video=nil
sceneGroup:insert(video)
im.isHitTestable=false

end






local function facebook()
system.openURL( "http://www.facebook.com" )
end

local function cambiaScena5 ()
composer.gotoScene("flydream")
end

local function avviamusica()
audio.play(backsound,{channel=1,loops=-1})


local function pausavideo(event)
video:pause()
end

end

--video:addEventListener("system",onScreenTouch)

local function fermamusica(event)

if(event.phase=="began") then
appmusic=appmusic+1
if(appmusic%2==0) then
audio.stop()
elseif(appmusic%2==1) then
avviamusica()

print(appmusic)
end
end
end




-- create()

function scene:create( event )

local sceneGroup = self.view
-- Code here runs when the scene is first created but has not yet appeared on screen
background = display.newImageRect( sceneGroup, "sfondoo.png", 800, 550 ) --1024x600
background.x = display.contentCenterX-50
background.y = display.contentCenterY-48
sceneGroup:insert(background)
backsound=audio.loadStream("swamp.wav")

stopsound=display.newImageRect("stopmusic.png",65,65)
stopsound.x=display.contentCenterX-351
stopsound.y=display.contentCenterY-183
sceneGroup:insert(stopsound)


newGameButton = display.newRect( 250, 430, 235, 40 )
newGameButton.alpha = 0
newGameButton.isHitTestable = true
sceneGroup:insert(newGameButton)

newGameButton3=display.newRect(260,480,300,40)
newGameButton3.alpha = 0
newGameButton3.isHitTestable = true
sceneGroup:insert(newGameButton3)

newGameButton2 = display.newRect( 280, 410, 235, 40 )
newGameButton2.alpha = 0
newGameButton2.isHitTestable = true
sceneGroup:insert(newGameButton2)


ultimo=display.newRect(280,530,235,40)
ultimo.alpha=0
ultimo.isHitTestable=true
sceneGroup:insert(ultimo)


--temporaneo=display.newRect( 180, 310, 135, 80 )
--temporaneo.alpha = 4
--temporaneo.isHitTestable = true
--sceneGroup:insert(temporaneo)

--nuovo=display.newImageRect("new.png",100,100)
--nuovo.x=display.contentCenterX-100
--nuovo.y=display.contentCenterY+100

--playButton = display.newText( sceneGroup, "Play", display.contentCenterX, 500, native.systemFont, 60 )
-- playButton:setFillColor( 0, 0, 0 )
-- sceneGroup:insert(playButton)

-- playButton:addEventListener( "tap", cambiaScena )
playcredit=display.newRect(display.contentCenterX,display.contentCenterY,80,120)
playcredit.x=display.contentCenterX+330
playcredit.y=display.contentCenterY+115
playcredit.alpha=0
playcredit.isHitTestable=true
sceneGroup:insert(playcredit)

logo=display.newImageRect("logovf.png",250,250)
logo.x=display.contentCenterX+333
logo.y=display.contentCenterY+130
sceneGroup:insert(logo)


fb=display.newImageRect(sceneGroup,"fb.png",110,110)
fb.x=display.contentCenterX+335
fb.y=display.contentCenterY-165

--video:load("intro.mp4",system.DocumentsDirectory)
--video:play()
sceneGroup:insert(fb)



--imm=display.newRect(display.contentCenterX,display.contentCenterY,800,800)
--imm.alpha=0
--imm.isHitTestable=true
--imm:addEventListener("tap",onScreensTouch)





--media.playVideo( "intro.mp4", true, onComplete )
--video:addEventListener("touch",pausavideo)
--video:load("intro.mp4")
--video:play()







--video:load("intro.mp4")
--video:play()
--video:addEventListener("tap",onScreenTouch)
--im=display.newRect(display.contentCenterX,display.contentCenterY,800,800)
--im.alpha=0
--im.isHitTestable=true
--im:addEventListener("tap",onScreenTouch)


torcia=display.newImageRect(sceneGroup,"torcia.png",75,70)
torcia.x=display.contentCenterX-130
torcia.y=display.contentCenterY+85

--vibrator.vibrate(3000)

--vibrator.vibrate({100, 500,  200, 250}, 1)


end


-- show()
function scene:show( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
if(appmusica==1) then
avviamusica()
end
--Code here runs when the scene is still off screen (but is about to come on screen)
elseif ( phase == "did" ) then

newGameButton:addEventListener( "tap", cambiaScena4 )
playcredit:addEventListener("tap",cambiaScena2)
stopsound:addEventListener("touch",fermamusica)
newGameButton3:addEventListener("tap",cambiaScena)
--newGameButton2:addEventListener("tap",cambiaScena)
--temporaneo:addEventListener("tap",cambiaScena3)
--nuovo:addEventListener("tap",cambiaScena5)
--video:load("intro.mp4",system.DocumentsDirectory)
--video:play()
torcia:addEventListener("tap",accendiTorcia)

ultimo:addEventListener("tap",videocredits)

fb:addEventListener("tap",facebook)
-- Code here runs when the scene is entirely on screen
end
end


-- hide()
function scene:hide( event )

local sceneGroup = self.view
local phase = event.phase

if ( phase == "will" ) then
-- Code here runs when the scene is on screen (but is about to go off screen)

elseif ( phase == "did" ) then
-- Code here runs immediately after the scene goes entirely off screen
audio.stop()
audio.dispose( backsound )
backsound = nil
--nuovo.isVisible=false
--composer.removeScene("menu")
end
end


-- destroy()
function scene:destroy( event )

local sceneGroup = self.view
-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene
